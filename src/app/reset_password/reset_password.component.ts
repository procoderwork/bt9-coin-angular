import {Component, OnInit, ElementRef, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormControl, AbstractControl} from '@angular/forms';
import {Notifications} from "../services/notifications.service";
import {Api} from "../services/api.service";
import {ActivatedRoute, Params, Router} from "@angular/router";
import {SettingsService} from "../services/settings/settings.service";
import {BalanceService} from "../services/balance.service";
import {Validate} from "../services/validate.service";
import {ReCaptchaComponent} from "angular2-recaptcha";
import {PasswordValidation} from "../shared/components/password-validator.component";

declare var $: any;

@Component({
    selector: 'app-resetpassword-cmp',
    templateUrl: './reset_password.component.html',
    styleUrls: ['./reset_password.component.scss']
})

export class ResetPasswordComponent implements OnInit {
    reset: FormGroup;
    user: any = {};
    sub: any;

    constructor(private element: ElementRef,
                private formBuilder: FormBuilder,
                public api: Api,
                public notify: Notifications,
                public router: Router,
                public settings: SettingsService,
                public balance: BalanceService,
                public validate: Validate,
                private activatedRoute: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.sub = this.activatedRoute.data
            .subscribe(data => {
                this.user.id = data.user_id;
            });

        setTimeout(function () {
            $('.card').removeClass('card-hidden');
        }, 700);

        this.reset = this.formBuilder.group({
            password: ['', Validators.required],
            confirmPassword: ['', Validators.required],
        }, {
            validator: PasswordValidation.MatchPassword // your validation method
        });
    }
    onResetPassword() {
        if (this.reset.valid) {
            this.api.resetPassword(this.user).subscribe(res => {
                if (res.success) {
                    this.notify.showNotification('Password was changed succesfully', 'top', 'center', 'success');
                    this.router.navigate(['login']);
                } else {
                    this.notify.showNotification(res.error, 'top', 'center', 'warning');
                }
            }, err => {
                this.notify.showNotification('Error', 'top', 'center', 'danger');
            });
        } else {
            this.validate.validateAllFormFields(this.reset);
        }
    }
}
