import {Injectable} from '@angular/core';
import {Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot} from '@angular/router';
import {Api} from "../services/api.service";

@Injectable()
export class SponserResolver implements Resolve<any> {

    constructor(private api: Api,
                private router: Router) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<any> {
        let sponser = route.params['sponser'];

        return new Promise((resolve, reject) => {
            this.api.getSponser({sponser: sponser}).subscribe(res => {
                if (res.success) {
                    resolve(res.sponser);
                } else {
                    this.router.navigate(['login']);
                    resolve(null);
                }
            }, err => {
                this.router.navigate(['login']);
                resolve(null);
            });
        });
    }
}