import {Component, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormControl, AbstractControl} from '@angular/forms';
import {PasswordValidation} from '../shared/components/password-validator.component';
import {Api} from '../services/api.service';
import {Notifications} from '../services/notifications.service';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {Validate} from "../services/validate.service";
import {ReCaptchaComponent} from "angular2-recaptcha";

@Component({
    selector: 'app-register-cmp',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})

export class RegisterComponent implements OnInit {
    @ViewChild(ReCaptchaComponent) captcha: ReCaptchaComponent;

    signup: FormGroup;
    user: any = {
        agree: true
    };

    sysSettings: any = {};

    loading = false;
    constructor(
        private formBuilder: FormBuilder,
        public router: Router,
        public api: Api,
        public notify: Notifications,
        private activatedRoute: ActivatedRoute,
        public validate: Validate
    ) {
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe((params: Params) => {
            this.user.sponser = params['sponser'];
        });
        this.signup = this.formBuilder.group({
            // To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
            username: [null, [Validators.required, Validators.pattern('^[a-zA-Z0-9]+$')]],
            fullname: [null, [Validators.required, Validators.pattern('^[a-z A-Z]+$')]],
            email: [null, [Validators.required, Validators.pattern("^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
            // We can use more than one validator per field. If we want to use more than one validator we have to wrap our array of validators with a Validators.compose function. Here we are using a required, minimum length and maximum length validator.
            password: ['', Validators.required],
            confirmPassword: ['', Validators.required],
            sponser: [''],
            agree: [''],
        }, {
            validator: PasswordValidation.MatchPassword // your validation method
        });

        this.api.getSettings().subscribe( res => {
            if (res.success) {
                this.sysSettings = res.data;
            }
        });
    }

    onSignup() {
        if (this.signup.valid) {
            if (!this.user.agree) {
                this.notify.showNotification('Please check', 'top', 'center', 'danger');
                return;
            }

            if (this.sysSettings.register_recaptcha == 1 && (this.captcha.getResponse() == null || this.captcha.getResponse() == '')) {
                this.notify.showNotification('Captcha validation failed.', 'top', 'center', 'danger');
                return;
            }

            this.loading = true;
            this.api.register(this.user).subscribe(res => {
                this.loading = false;
                if (res.success) {
                    this.notify.showNotification('Success', 'top', 'center', 'success');
                    this.router.navigate(['/login']);
                } else {
                    this.notify.showNotification(res.error, 'top', 'center', 'warning');
                }
            }, err => {
                this.loading = false;
                this.notify.showNotification('Error', 'top', 'center', 'danger');
            });
        } else {
            this.validate.validateAllFormFields(this.signup);
        }
    }

    handleCorrectCaptcha($event) {
        console.log($event);
    }
}
