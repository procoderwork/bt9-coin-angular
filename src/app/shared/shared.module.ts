import {NgModule, ModuleWithProviders} from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {FooterModule} from "./footer/footer.module";
import {NavbarModule} from "./navbar/navbar.module";
import {FieldErrorDisplayComponent} from "./components/field-error-display/field-error-display.component";
import {ModalDialogComponent} from "./components/dialog/modal.dialog.component";
import {LoaderComponent} from "./components/loader/loader.component";
import {LoadingComponent} from "./components/loading/loading.component";

@NgModule({
    exports: [
        FooterModule,
        NavbarModule,
        FormsModule,
        ReactiveFormsModule,
        FieldErrorDisplayComponent,
        ModalDialogComponent,
        LoaderComponent,
        LoadingComponent
    ],
    imports: [
        CommonModule,
        FooterModule,
        NavbarModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [
        FieldErrorDisplayComponent,
        ModalDialogComponent,
        LoaderComponent,
        LoadingComponent
    ]
})

// https://github.com/ocombe/ng2-translate/issues/209
export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule
        };
    }
}
