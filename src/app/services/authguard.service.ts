/**
 * Created by ApolloYr on 11/17/2017.
 */
import {Injectable} from '@angular/core';
import {Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot} from '@angular/router';
import {SettingsService} from './settings/settings.service';
import {Api} from './api.service';
import {BalanceService} from "./balance.service";

@Injectable()
export class AuthGuard implements Resolve<any> {

    constructor(private router: Router,
                private settings: SettingsService,
                private api: Api,
                private balance: BalanceService
    ) {
    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.settings.getUserSetting('name')) {
                this.canAccess(state.url, this.settings.getUserSetting('role')) ? resolve(true) : reject('no privillege');
            } else if (this.settings.getStorage('token')) {
                this.api.getAccountInfo().subscribe(res => {
                    if (!this.canAccess(state.url, res.role)) {
                        reject('no privillege');
                    } else {
                        this.settings.setAppSetting('is_loggedin', true);
                        this.settings.user = res;

                        this.balance.init();

                        resolve(true);
                    }
                }, err => {
                    reject('information is invalid');
                    this.settings.setStorage('token', false);
                    this.router.navigate(['/login']);
                });
            } else {
                reject('not logged in');
                this.router.navigate(['/login']);
            }
        });
    }

    private canAccess(url, role) {
        if (url.startsWith('/admin') && role == 'admin') {
            return true;
        } else if (url.startsWith('/admin') && role != 'admin') {
            return false;
        } else {
            return true;
        }
    }
}

