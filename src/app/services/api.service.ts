/**
 * Created by ApolloYr on 11/17/2017.
 */

import {Injectable} from '@angular/core';
import {Http, Headers, URLSearchParams} from '@angular/http';
import {Observable} from 'rxjs';
import {SettingsService} from './settings/settings.service';
import {Router} from '@angular/router';

@Injectable()
export class Api {
    constructor(private http: Http,
                private router: Router,
                public settings: SettingsService) {
    }

    createAuthorizationHeader(headers: Headers) {
        headers.append('Authorization', 'Bearer ' + this.settings.getStorage('token'));
    }

    get(url, data?) {
        let headers = new Headers();
        this.createAuthorizationHeader(headers);

        const params: URLSearchParams = new URLSearchParams();
        if (data) {
            for (var key in data) {
                params.set(key, data[key]);
            }
        }

        return this.http.get(this.settings.apiUrl + url, {
            headers: headers,
            search: params
        }).map(res => res.json()).catch((error: any) => this.handleError(this, error));
    }

    post(url, data) {
        let headers = new Headers();
        this.createAuthorizationHeader(headers);

        return this.http.post(this.settings.apiUrl + url, data, {
            headers: headers
        }).map(res => res.json()).catch((error: any) => this.handleError(this, error));
    }

    put(url, data) {
        let headers = new Headers();
        this.createAuthorizationHeader(headers);

        return this.http.put(this.settings.apiUrl + url, data, {
            headers: headers
        }).map(res => res.json()).catch((error: any) => this.handleError(this, error));
    }

    uploadFile(file) {
        let headers = new Headers();
        this.createAuthorizationHeader(headers);

        var formData = new FormData();

        formData.append("file", file, file.name);

        return this.http.post(this.settings.apiUrl + '/upload/file', formData, {
            headers: headers
        }).map(res => res.json()).catch((error: any) => this.handleError(this, error));
    }

    handleError(_parent, error: any) {
        if ((error.status == 401 || error.status == 400) && error.url && !error.url.endsWith('/login')) {
            //console.log('unauthorized');
            //if (_parent.settings) _parent.settings.setStorage('token', false);
            //if (_parent.settings) _parent.settings.setAppSetting('is_loggedin', false);
            //_parent.router.navigate(['/login']);
        }
        // In a real world app, you might use a remote logging infrastructure

        return Observable.throw(error);
    }

    getAccountInfo() {
        return this.get('/account');
    }

    login(data): any {
        return this.post('/login', data);
    }

    resetPassword(data) {
        return this.post('/reset_password', data);
    }

    register(data): any {
        return this.post('/register', data);
    }

    getCoinAddress(coin, data) {
        return this.get('/' + coin + '/address', data);
    }

    getCoinBalance(coin, data) {
        return this.get('/' + coin + '/balance', data);
    }

    withdrawCoin(coin, data) {
        return this.post('/' + coin + '/withdraw', data);
    }

    cancelWithdrawCoin(coin, data) {
        return this.post('/' + coin + '/withdraw/cancel', data);
    }

    getSentTxList(coin, data) {
        return this.get('/' + coin + '/senttx', data);
    }

    getReceivedTxList(coin, data) {
        return this.get('/' + coin + '/receivedtx', data);
    }

    getUnconfirmedReceivedTxList(coin, data) {
        return this.get('/' + coin + '/unconfirmed/receivedtx', data);
    }

    getUnconfirmedSentTxList(coin, data) {
        return this.get('/' + coin + '/unconfirmed/senttx', data);
    }

    getTxList(coin, data) {
        return this.get('/' + coin + '/gettx', data);
    }



    getReferral(data) {
        return this.get('/get_referral', data);
    }

    getCoinRate(data) {
        return this.get('/rate', data);
    }

    getTokenBalance(data) {
        return this.get('/token_balance', data);
    }

    getUsdBalance(data) {
        return this.get('/usd_balance', data);
    }

    getTokenRate(data) {
        return this.get('/token_rate', data);
    }

    getBonusList(data) {
        return this.get('/bonus_lists', data);
    }

    getTotalSold(data) {
        return this.get('/toten_total', data);
    }

    buyTokenWithBTC(data) {
        return this.post('/buyTokenWithBTC', data);
    }

    buyTokenWithETH(data) {
        return this.post('/buyTokenWithETH', data);
    }

    getG2FCode(data) {
        return this.get('/getG2FCode', data);
    }

    setG2F(data) {
        return this.post('/setG2F', data);
    }

    confirmG2FCode(data) {
        return this.post('/confirmG2FCode', data);
    }

    sendWithdrawMail(data) {
        return this.post('/withdraw/sendmail', data);
    }
    confirmWithdrawCode(data) {
        return this.post('/withdraw/confirmcode', data);
    }

    confirmG2FCodeWithoutlogin(data) {
        return this.post('/confirmG2FCodeWithoutlogin', data);
    }

    chagePwd(data) {
        return this.post('/change_password', data);
    }

    changeUserInfo(data) {
        return this.post('/account/changeLoginEmailStatus', data);
    }

    changeWithdrawMailStatus(data) {
        return this.post('/account/changeWithdrawMailStatus', data);
    }

    getNetworkFee(coin, data) {
        return this.get('/' + coin + '/network_fee', data);
    }

    getSettings() {
        return this.get('/settings', {});
    }

    getTokenHistory() {
        return this.get('/token_history', {});
    }

    sendForgotEmail(data) {
        return this.post('/account/sendForgotEmail', data);
    }

    sendActivateEmail(data) {
        return this.post('/account/sendActivateEmail', data);
    }

    confirmResetCode(data) {
        return this.get('/confirm_resetcode', data);
    }

    getSponser(data) {
        return this.get('/get_sponser', data);
    }

    getLoginHistory(data) {
        return this.get('/account/loginHistory', data);
    }

    /// Admin Apis
    getAdminUsers(data) {
        return this.get('/admin/adminUsers', data);
    }
    addAdmin(data) {
        return this.post('/admin/add/adminUser', data);
    }
    deleteAdmin(data) {
        return this.post('/admin/delete/adminUser', data);
    }
    userCount(data) {
        return this.get('/admin/user/count', data);
    }
    getSystemCurrency(data) {
        return this.get('/admin/sysCurrency', data);
    }
    getMyAltCoinChart() {
        return this.get('/admin/token_chart', {});
    }
    getSystemCurrencyChart() {
        return this.get('/admin/syscurrency_chart', {});
    }
    getUsers() {
        return this.get('/admin/users', {});
    }
    getUserIDByEmail(data) {
        return this.get('/admin/user/getIdByEmail', data);
    }
    getUser(data) {
        return this.get('/admin/user/get', data);
    }
    deleteUser(data) {
        return this.post('/admin/user/delete', data);
    }
    getAllTokenHistory(data) {
        return this.get('/admin/token_history', data);
    }
    getAdminUserTokenHistory(data) {
        return this.get('/admin/user/BT9/history', data);
    }
    getAdminUserBTCHistory(data) {
        return this.get('/admin/user/BTC/history', data);
    }
    getAdminUserETHHistory(data) {
        return this.get('/admin/user/ETH/history', data);
    }
    getAllTxList(coin, data) {
        return this.get('/admin/' + coin + '/transactions', data);
    }
    saveBonus(data) {
        return this.post('/admin/bonus_lists', data);
    }
    saveRates(data) {
        return this.post('/admin/token_rates', data);
    }
    saveSettings(data) {
        return this.post('/admin/settings', data);
    }
    addMyAltCoin(data) {
        return this.post('/admin/exchange/add', data);
    }

    // ICO Agenda

    getICOAgenda(data) {
        return this.get('/ico_agenda', data);
    }
    saveICOAgenda(data) {
        return this.post('/ico_agenda', data);
    }
    deleteICOAgenda(data) {
        return this.post('/ico_agenda/delete', data);
    }
    getICOAgendaChart(data) {
        return this.get('/ico_agenda/chart', data);
    }

    // BTC/MyAltCoin Exchange
    makeBTCBuyOrder(data) {
        return this.post('/order/btc/buy', data);
    }

    makeBTCSellOrder(data) {
        return this.post('/order/btc/sell', data);
    }

    getBTCBuyOrders(data) {
        return this.get('/order/btc/buy', data);
    }

    getBTCSellOrders(data) {
        return this.get('/order/btc/sell', data);
    }

    getBTCPastOrders(data) {
        return this.get('/order/btc/past', data);
    }

    getBTCOpenOrders(data) {
        return this.get('/order/btc/open', data);
    }

    cancelBTCOrder(data) {
        return this.post('/order/btc/delete', data);
    }

    // ETH/MyAltCoin Exchange
    makeETHBuyOrder(data) {
        return this.post('/order/eth/buy', data);
    }

    makeETHSellOrder(data) {
        return this.post('/order/eth/sell', data);
    }

    getETHBuyOrders(data) {
        return this.get('/order/eth/buy', data);
    }

    getETHSellOrders(data) {
        return this.get('/order/eth/sell', data);
    }

    getETHPastOrders(data) {
        return this.get('/order/eth/past', data);
    }

    getETHOpenOrders(data) {
        return this.get('/order/eth/open', data);
    }

    cancelETHOrder(data) {
        return this.post('/order/eth/delete', data);
    }

    // Lending
    getLendingSetting(data) {
        return this.get('/lending/setting', data);
    }
    saveLendingSetting(data) {
        return this.post('/lending/setting', data);
    }
    getLending(data) {
        return this.get('/lending', data);
    }
    getLendingTx(data) {
        return this.get('/lending/tx', data);
    }
    saveLending(data) {
        return this.post('/lending', data);
    }
    deleteLendingSetting(data) {
        return this.post('/lending/setting/delete', data);
    }
    getAllLending(data) {
        return this.get('/lending/admin', data);
    }
    getLendingBalance(data) {
        return this.get('/lending/balance', data);
    }
    getLendingDaily(data) {
        return this.get('/lending/daily', data);
    }
    transferLending(data) {
        return this.post('/lending/transfer', data);
    }
    transferDailyLending(data) {
        return this.post('/lending/transfer/daily', data);
    }
    getInterestDaily(data) {
        return this.get('/lending/interest', data);
    }
    releaseAllLending(data) {
        return this.post('/lending/release/all', data);
    }
    // Staking
    getStaking(data) {
        return this.get('/staking', data);
    }
    saveStaking(data) {
        return this.post('/staking', data);
    }
    getAllStaking(data) {
        return this.get('/staking/admin', data);
    }
    getStakingBalance(data) {
        return this.get('/staking/balance', data);
    }

    releaseAllStaking(data) {
        return this.post('/staking/release/all', data);
    }
    // ICO History
    getAllICOHistory(data) {
        return this.get('/admin/ico/history', data);
    }

    // Admin Withdraw
    getWithdrawPending(coin) {
        return this.get('/admin/withdraw/' + coin + '/pending', {});
    }
    getWalletTotalBalance(coin) {
        return this.get('/admin/wallet/' + coin + '/balance', {});
    }
    estimateFee(coin, data) {
        return this.post('/admin/' + coin + '/withdraw/estimate_fee', data);
    }
    withdrawPending(coin, data) {
        return this.post('/admin/' + coin + '/withdraw', data);
    }

    // Statistics
    getUserStatistics(data) {
        return this.get('/admin/user/statistics', data);
    }
    getICOStatistics(data) {
        return this.get('/admin/ico/statistics', data);
    }
    getLendingStatistics(data) {
        return this.get('/admin/lending/statistics', data);
    }
    getStakingStatistics(data) {
        return this.get('/admin/staking/statistics', data);
    }
    getFeeStatistics(data) {
        return this.get('/admin/fee/statistics', data);
    }
    getAllTransactionHistory(coin, data) {
        return this.get('/admin/' + coin + '/transactions', data);
    }
    getExchangeStatistics(coin, data) {
        return this.get('/exchange/' + coin + '/statistics', data);
    }
    getExchangeHistory(coin, data) {
        return this.get('/exchange/' + coin + '/history', data);
    }

    // Ticket
    getMyTicket(data) {
        return this.get('/ticket', data);
    }
    getAllTicket(data) {
        return this.get('/ticket/all', data);
    }
    getTicketDetail(data) {
        return this.get('/ticket/detail', data);
    }
    postTicket(data) {
        return this.post('/ticket', data);
    }
    replyTicket(data) {
        return this.post('/ticket/reply', data);
    }
    closeTicket(data) {
        return this.post('/ticket/close', data);
    }
}

