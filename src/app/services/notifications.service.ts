/**
 * Created by ApolloYr on 11/18/2017.
 */

import {Injectable} from '@angular/core';

declare const $: any;

@Injectable()
export class Notifications {
    constructor() {

    }
    showNotification(message: any, from: any, align: any, type: any) {
        // const type = ['', 'info', 'success', 'warning', 'danger', 'rose', 'primary'];

        const color = Math.floor((Math.random() * 6) + 1);

        $.notify({
            icon: 'notifications',
            message: message
        }, {
            type: type,
            timer: 1000,
            placement: {
                from: from,
                align: align
            }
        });
    }
}
