/**
 * Created by ApolloYr on 11/17/2017.
 */

import {Injectable} from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormControl, AbstractControl} from '@angular/forms';
import {Api} from "./api.service";
import {SettingsService} from "./settings/settings.service";

@Injectable()
export class BalanceService {
    constructor(
        public api: Api,
        public settings: SettingsService
    ) {
    }

    init() {
        if (this.settings.getUserSetting('role') == 'admin') {
            return;
        }

        this.api.getSettings().subscribe( res => {
            if (res.success) {
                this.settings.sys = res.data;
            }
        });
        this.getBtcAddress();
        this.getBt9Address();
        this.getEthAddress();
        this.getTokenBalance();
        // this.getUsdBalance();
    }

    getBtcAddress() {
        this.api.getCoinAddress('BTC', {}).subscribe( res => {
            if (res.success) {
                this.settings.setUserSetting('btc_address', res.address);
                this.getBtcBalance();
                // this.getEthAddress();
            }
        }, err => {

        });
    }


    getBt9Address() {
        this.api.getCoinAddress('BT9', {}).subscribe( res => {
            if (res.success) {
                this.settings.setUserSetting('bt9_address', res.address);
            }
        }, err => {

        });
    }

    getBtcBalance() {
        this.api.getCoinBalance('BTC', {}).subscribe( res => {
            if (res.success) {
                this.settings.setUserSetting('btc_balance', res.available_balance);
            }
        }, err => {

        });
    }

    getEthAddress() {
        this.api.getCoinAddress('ETH', {}).subscribe( res => {
            if (res.success) {
                this.settings.setUserSetting('eth_address', res.address);
                this.getEthBalance();
            }
        }, err => {

        });
    }

    getEthBalance() {
        this.api.getCoinBalance('ETH', {}).subscribe( res => {
            if (res.success) {
                this.settings.setUserSetting('eth_balance', res.available_balance);
            }
        }, err => {

        });
    }

    getTokenBalance() {
        this.api.getTokenBalance({}).subscribe( res => {
            if (res.success) {
                this.settings.setUserSetting('token_balance', res.balance);
            }
        }, err => {

        });
    }

    getUsdBalance() {
        this.api.getUsdBalance({}).subscribe( res => {
            if (res.success) {
                this.settings.setUserSetting('usd_balance', res.balance);
            }
        }, err => {

        });
    }
}

