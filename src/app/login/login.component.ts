import {Component, OnInit, ElementRef, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormControl, AbstractControl} from '@angular/forms';
import {Notifications} from "../services/notifications.service";
import {Api} from "../services/api.service";
import {ActivatedRoute, Router} from "@angular/router";
import {SettingsService} from "../services/settings/settings.service";
import {BalanceService} from "../services/balance.service";
import {Validate} from "../services/validate.service";
import {ReCaptchaComponent} from "angular2-recaptcha";

declare var $: any;

@Component({
    selector: 'app-login-cmp',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
    @ViewChild(ReCaptchaComponent) captcha: ReCaptchaComponent;
    login: FormGroup;
    user: any = {};

    showAuthModal = false;
    authCode = '';
    codeMatch = true;

    g2f_code = '';
    res: any = {};

    unconfirmed = 0;

    showMailModal = false;
    mail_type = '';
    confirm_email = '';
    error = '';

    sendingEmail = false;
    loging = false;

    sysSettings: any = {};
    constructor(private element: ElementRef,
                private formBuilder: FormBuilder,
                public api: Api,
                public notify: Notifications,
                public router: Router,
                public settings: SettingsService,
                public balance: BalanceService,
                public validate: Validate
    ) {
    }

    ngOnInit() {
        if (this.router.url.includes('/confirmed')) {
            this.notify.showNotification('Your account was confirmed successfully.', 'top', 'center', 'success');
        }

        setTimeout(function () {
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
        }, 700);

        this.login = this.formBuilder.group({
            email: [null, [Validators.required, Validators.pattern("^[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
            password: ['', Validators.required],
        });

        this.api.getSettings().subscribe( res => {
            if (res.success) {
                this.sysSettings = res.data;
            }
        });
    }

    successLogin() {
        this.settings.user = this.res;
        this.settings.setStorage('token', this.res.token);

        this.balance.init();
        if (this.res.role == 'admin') {
            this.router.navigate(['/admin/dashboard']);
        } else {
            this.router.navigate(['/dashboard']);
        }
    }

    onLogin() {
        if (this.login.valid) {
            if (this.sysSettings.login_recaptcha == 1 && (this.captcha.getResponse() == null || this.captcha.getResponse() == '')) {
                this.notify.showNotification('Captcha validation failed.', 'top', 'center', 'danger');
                return;
            }

            this.loging = true;
            this.api.login(this.user).subscribe(res => {
                if (res.success) {
                    this.loging = false;
                    if (res.confirmed == 0 || res.confirmed == null) {
                        this.unconfirmed = 1;
                        return;
                    }

                    this.res = res;
                    if (res.allow_g2f == 1) {
                        this.g2f_code = res.g2f_code;
                        this.showAuthModal = true;
                    } else {
                        this.successLogin();
                    }
                } else {
                    this.loging = false;
                    this.notify.showNotification(res.error, 'top', 'center', 'warning');
                }
            }, err => {
                this.loging = false;
                this.notify.showNotification('Error', 'top', 'center', 'danger');
            });
        } else {
            this.validate.validateAllFormFields(this.login);
        }
    }

    submitCode() {
        if (this.authCode == '') {
            return;
        }
        this.codeMatch = true;
        this.api.confirmG2FCodeWithoutlogin({
            code: this.authCode,
            g2f_code: this.g2f_code
        }).subscribe( res => {
            if (res.success) {
                this.successLogin();
            } else {
                this.codeMatch = false;
            }
        }, err => {
            this.codeMatch = false;
        });
    }

    handleCorrectCaptcha($event) {
        console.log($event);
    }

    forgot() {
        this.mail_type = 'forgot';
        this.showMailModal = true;
    }

    resend() {
        this.mail_type = 'resend';
        this.showMailModal = true;
    }

    submitEmail() {
        if (this.confirm_email == '') {
            this.notify.showNotification('Please enter email address', 'top', 'center', 'danger');
            return;
        }

        this.error = '';

        if (this.mail_type == 'forgot') {
            this.sendingEmail = true;
            this.api.sendForgotEmail({
                email: this.confirm_email
            }).subscribe( res => {
                if (res.success) {
                    this.sendingEmail = false;
                    this.showMailModal = false;
                    this.notify.showNotification('Email was sent successfully. Please check your email.', 'top', 'center', 'success');
                } else {
                    this.sendingEmail = false;
                    this.error = res.error;
                }
            }, err => {
                this.sendingEmail = false;
                this.notify.showNotification('Error', 'top', 'center', 'danger');
            });
        } else if (this.mail_type == 'resend') {
            this.sendingEmail = true;
            this.api.sendActivateEmail({
                email: this.confirm_email
            }).subscribe( res => {
                if (res.success) {
                    this.sendingEmail = false;
                    this.showMailModal = false;
                    this.notify.showNotification('Confirmation email was sent successfully. Please check your email.', 'top', 'center', 'success');
                } else {
                    this.sendingEmail = false;
                    this.error = res.error;
                }
            }, err => {
                this.sendingEmail = false;
                this.notify.showNotification('Error', 'top', 'center', 'danger');
            });
        }
    }
}
