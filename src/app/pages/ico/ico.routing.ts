import { Routes } from '@angular/router';

import {IcoComponent} from './ico.component';

export const IcoRoutes: Routes = [
    {
        path: '',
        component: IcoComponent
    }
];
