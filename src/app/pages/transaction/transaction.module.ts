import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MdModule } from '../../md/md.module';
import { MaterialModule } from '../../app.module';

import {TransactionComponent} from './transaction.component';
import {TransactionRoutes} from './transaction.routing';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(TransactionRoutes),
        FormsModule,
        MdModule,
        MaterialModule
    ],
    declarations: [TransactionComponent]
})

export class TransactionModule {}
