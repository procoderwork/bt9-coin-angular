import {Component, OnInit, AfterViewInit} from '@angular/core';
import {TableData} from '../../md/md-table/md-table.component';
import {LegendItem, ChartType} from '../../md/md-chart/md-chart.component';

import * as Chartist from 'chartist';
import {Api} from "../../services/api.service";

declare const $: any;

@Component({
    selector: 'app-transaction',
    templateUrl: './transaction.component.html'
})
export class TransactionComponent implements OnInit {
    btcTxList: any = [];
    ethTxList: any = [];
    altcoinTxList: any = [];
    lendingList: any = [];

    altcoindatatable: any = null;
    btcdatatable: any = null;
    ethdatatable: any = null;
    lendingdatatable: any = null;

    constructor(
        public api: Api
    ) {}

    ngOnInit() {
        this.btcLoadTxList();
        // this.ethLoadTxList();
        // this.altcoinLoadTxList();
        // this.lendingLoad();
    }

    buildBTCDataTable() {
        this.btcdatatable = $('#btcdatatables').DataTable({
            'pagingType': 'full_numbers',
            'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
            responsive: true,
            language: {
                search: '_INPUT_',
                searchPlaceholder: 'Search records',
            }
        });

    }

    buildETHDataTable() {
        this.ethdatatable = $('#ethdatatables').DataTable({
            'pagingType': 'full_numbers',
            'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
            responsive: true,
            language: {
                search: '_INPUT_',
                searchPlaceholder: 'Search records',
            }
        });
    }

    buildAltCoinDataTable() {
        this.altcoindatatable = $('#altcoindatatables').DataTable({
            'pagingType': 'full_numbers',
            'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
            responsive: true,
            language: {
                search: '_INPUT_',
                searchPlaceholder: 'Search records',
            }
        });
    }

    buildLendingDataTable() {
        this.lendingdatatable = $('#lendingdatatables').DataTable({
            'pagingType': 'full_numbers',
            'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
            responsive: true,
            language: {
                search: '_INPUT_',
                searchPlaceholder: 'Search records',
            }
        });

    }

    btcLoadTxList() {

        if (this.btcdatatable) {
            this.btcdatatable.destroy();
        }
        this.api.getTxList('BTC', {}).subscribe( res => {
            if (res.success) {
                this.btcTxList = res.data;

                let _parent = this;
                var timerID = setInterval(function() {
                    if ($('#btcdatatables tbody tr').length == res.data.length) {
                        clearInterval(timerID);
                        _parent.buildBTCDataTable();
                    }
                }, 200);
            }
        }, err => {

        });
    }

    ethLoadTxList() {

        if (this.ethdatatable) {
            this.ethdatatable.destroy();
        }
        this.api.getTxList('ETH', {}).subscribe( res => {
            if (res.success) {
                this.ethTxList = res.data;

                let _parent = this;
                var timerID1 = setInterval(function() {
                    if ($('#ethdatatables tbody tr').length == res.data.length) {
                        clearInterval(timerID1);
                        _parent.buildETHDataTable();
                    }
                }, 200);
            }
        }, err => {

        });
    }

    altcoinLoadTxList() {
        if (this.altcoindatatable) {
            this.altcoindatatable.destroy();
        }

        this.api.getTokenHistory().subscribe( res => {
            if (res.success) {
                this.altcoinTxList = res.data;

                let _parent = this;
                var timerID1 = setInterval(function() {
                    if ($('#altcoindatatables tbody tr').length == res.data.length) {
                        clearInterval(timerID1);
                        _parent.buildAltCoinDataTable();
                    }
                }, 200);
            }
        }, err => {

        });
    }

    lendingLoad() {

        if (this.lendingdatatable) {
            this.lendingdatatable.destroy();
        }
        this.api.getLendingTx({}).subscribe( res => {
            if (res.success) {
                this.lendingList = res.data;

                let _parent = this;
                var timerID2 = setInterval(function() {
                    if ($('#lendingdatatables tbody tr').length == res.data.length) {
                        clearInterval(timerID2);
                        _parent.buildLendingDataTable();
                    }
                }, 200);
            }
        }, err => {

        });
    }
}
