import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MdModule} from '../../md/md.module';
import {MaterialModule} from '../../app.module';

import {WalletComponent} from './wallet.component';
import {WalletRoutes} from './wallet.routing';
import {SharedModule} from "../../shared/shared.module";

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(WalletRoutes),
        FormsModule,
        MdModule,
        MaterialModule,
        ReactiveFormsModule,
        SharedModule
    ],
    declarations: [WalletComponent]
})

export class WalletModule {
}
