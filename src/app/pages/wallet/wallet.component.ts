import {Component, OnInit, AfterViewInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormControl, AbstractControl} from '@angular/forms';
import {TableData} from '../../md/md-table/md-table.component';
import {LegendItem, ChartType} from '../../md/md-chart/md-chart.component';

import * as Chartist from 'chartist';
import {SettingsService} from "../../services/settings/settings.service";
import {Api} from "../../services/api.service";
import {Notifications} from "../../services/notifications.service";
import {Validate} from "../../services/validate.service";
import {BalanceService} from "../../services/balance.service";

declare const $: any;
declare var swal: any;
declare var web3: any;

@Component({
    selector: 'app-wallet',
    templateUrl: './wallet.component.html',
    styleUrls: ['./wallet.component.scss']
})
export class WalletComponent implements OnInit {
    withdrawBTCForm: FormGroup;
    withdrawETHForm: FormGroup;
    withdrawBT9Form: FormGroup;

    bitcoin: any = {
        amount: 0
    };
    ethereum: any = {
        amount: 0
    };
    bt9coin: any = {
        amount: 0
    };

    btcSentTxList: any = [];
    btcReceivedTxList: any = [];

    bt9SentTxList: any = [];
    bt9ReceivedTxList: any = [];

    ethSentTxList: any = [];
    ethReceivedTxList: any = [];

    authCode = '';
    codeMatch = true;
    showAuthModal = false;

    withdrawCoin = 'BTC';

    constructor(
        private formBuilder: FormBuilder,
        public settings: SettingsService,
        public api: Api,
        public notify: Notifications,
        public validate: Validate,
        public balance: BalanceService
    ) {}

    ngOnInit() {

        this.loadBTCSentTxList();
        this.loadBTCReceivedTxList();

        this.loadETHSentTxList();
        this.loadETHReceivedTxList();

        this.loadBT9SentTxList();
        this.loadBT9ReceivedTxList();

        this.withdrawBTCForm = this.formBuilder.group({
            address: ['', Validators.required],
            amount: ['', Validators.required],
        });

        this.withdrawETHForm = this.formBuilder.group({
            address: ['', Validators.required],
            amount: ['', Validators.required],
        });

        this.withdrawBT9Form = this.formBuilder.group({
            address: ['', Validators.required],
            amount: ['', Validators.required],
        });
    }

    loadBTCSentTxList() {
        this.api.getUnconfirmedSentTxList('BTC', {}).subscribe( res => {
           if (res.success) {
               this.btcSentTxList = res.data;
           }
        }, err => {

        });
    }

    loadBTCReceivedTxList() {
        this.api.getUnconfirmedReceivedTxList('BTC', {}).subscribe( res => {
            if (res.success) {
                this.btcReceivedTxList = res.data;
            }
        }, err => {

        });
    }

    loadBT9SentTxList() {
        this.api.getUnconfirmedSentTxList('BT9', {}).subscribe( res => {
            if (res.success) {
                this.bt9SentTxList = res.data;
            }
        }, err => {

        });
    }

    loadBT9ReceivedTxList() {
        this.api.getUnconfirmedReceivedTxList('BT9', {}).subscribe( res => {
            if (res.success) {
                this.bt9ReceivedTxList = res.data;
            }
        }, err => {

        });
    }

    loadETHSentTxList() {
        this.api.getUnconfirmedSentTxList('ETH', {}).subscribe( res => {
            if (res.success) {
                this.ethSentTxList = res.data;
            }
        }, err => {

        });
    }

    loadETHReceivedTxList() {
        this.api.getUnconfirmedReceivedTxList('ETH', {}).subscribe( res => {
            if (res.success) {
                this.ethReceivedTxList = res.data;
            }
        }, err => {

        });
    }

    ethAddressValidate(address) {
        if (!/^(0x)?[0-9a-f]{40}$/i.test(address)) {
            // check if it has the basic requirements of an address
            return false;
        } else if (/^(0x)?[0-9a-f]{40}$/.test(address) || /^(0x)?[0-9A-F]{40}$/.test(address)) {
            // If it's all small caps or all all caps, return true
            return true;
        } else {
            // Otherwise check each case
            return this.isChecksumAddress(address);
        }
    }

    isChecksumAddress(address) {
        // // Check each case
        // address = address.replace('0x', '');
        //
        // let addressHash = web3.sha3(address.toLowerCase());
        //
        // for (let i = 0; i < 40; i++ ) {
        //     // the nth letter should be uppercase if the nth digit of casemap is 1
        //     if ((parseInt(addressHash[i], 16) > 7 && address[i].toUpperCase() !== address[i]) || (parseInt(addressHash[i], 16) <= 7 && address[i].toLowerCase() !== address[i])) {
        //         return false;
        //     }
        // }
        return true;
    }

    cancelWithdraw(coin, id) {
        this.settings.loading = true;
        this.api.cancelWithdrawCoin(coin, {
            id: id
        }).subscribe( res => {
            this.settings.loading = false;
            if (res.success) {
                swal({
                    type: 'success',
                    title: 'Success',
                    text: 'Your withdraw has canceled successfully',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-success'
                });
                if (coin == 'BTC') {
                    this.balance.getBtcBalance();
                    this.loadBTCSentTxList();
                } else if (coin == 'ETH') {
                    this.balance.getEthBalance();
                    this.loadETHSentTxList();
                } else if (coin == 'BT9') {
                    this.balance.getTokenBalance();
                    this.loadBT9SentTxList();
                }
            } else {
                swal({
                    type: 'warning',
                    title: 'Error',
                    text: res.error,
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-warning'
                });
            }
        }, err => {
            this.settings.loading = false;
            swal({
                type: 'warning',
                title: 'Error',
                text: err._body,
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-warning'
            });
        });
    }

    withdraw(coin) {
        this.bitcoin.fee = this.settings.getSysSetting('withdraw_btc_fee');
        this.ethereum.fee = this.settings.getSysSetting('withdraw_eth_fee');
        this.bt9coin.fee = this.settings.getSysSetting('withdraw_bt9_fee');

        let params = this.bitcoin;
        if (coin == 'ETH') {
            params = this.ethereum;
        } else if (coin == 'BT9') {
            params = this.bt9coin;
        }

        this.settings.loading = true;
        this.api.withdrawCoin(coin, params).subscribe( res1 => {
            this.settings.loading = false;
            if (res1.success) {
                swal({
                    type: 'success',
                    title: 'Success',
                    text: 'Your withdraw has completed successfully',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-success'
                });
                if (coin == 'BTC') {
                    this.balance.getBtcBalance();
                    this.loadBTCSentTxList();
                } else if (coin == 'ETH') {
                    this.balance.getEthBalance();
                    this.loadETHSentTxList();
                } else if (coin == 'BT9') {
                    this.balance.getTokenBalance();
                    this.loadBT9SentTxList();
                }
            } else {
                swal({
                    type: 'warning',
                    title: 'Error',
                    text: res1.error,
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-warning'
                });
            }
        }, err => {
            this.settings.loading = false;
            swal({
                type: 'warning',
                title: 'Error',
                text: err._body,
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-warning'
            });
        });
    }

    onWithdrawBTC() {
        let _parent = this;
        if (this.withdrawBTCForm.valid) {
            if (Number(this.bitcoin.amount) < 0.001) {
                swal({
                    type: 'warning',
                    title: 'Error',
                    text: 'Minimum Withdrawal is 0.001 BTC',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-success'
                });
                return;
            }

            if (this.settings.getUserSetting('enable_withdrawmail') == 1) {

                this.withdrawCoin = 'BTC';
                this.sendMail();
            } else {
                this.withdraw('BTC');
            }
        } else {
            this.validate.validateAllFormFields(this.withdrawBTCForm);
        }
    }

    onWithdrawETH() {
        let _parent = this;
        if (this.withdrawETHForm.valid) {
            if (Number(this.ethereum.amount) < 0.005) {
                swal({
                    type: 'warning',
                    title: 'Error',
                    text: 'Minimum Withdrawal is 0.005 ETH',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-success'
                });
                return;
            }

            if (!this.ethAddressValidate(this.ethereum.address)) {
                swal({
                    type: 'warning',
                    title: 'Error',
                    text: 'Ethereum Address is not validate',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-warning'
                });
                return;
            }


            if (this.settings.getUserSetting('enable_withdrawmail') == 1) {
                this.sendMail();
                this.withdrawCoin = 'ETH';
            } else {
                this.withdraw('ETH');
            }

        } else {
            this.validate.validateAllFormFields(this.withdrawETHForm);
        }
    }

    onWithdrawBT9() {
        let _parent = this;
        if (this.withdrawBT9Form.valid) {
            if (Number(this.bt9coin.amount) < 0.005) {
                swal({
                    type: 'warning',
                    title: 'Error',
                    text: 'Minimum Withdrawal is 0.005 BT9',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-success'
                });
                return;
            }

            if (this.settings.getUserSetting('enable_withdrawmail') == 1) {
                this.sendMail();
                this.withdrawCoin = 'BT9';
            } else {
                this.withdraw('BT9');
            }

        } else {
            this.validate.validateAllFormFields(this.withdrawBT9Form);
        }
    }

    sendMail() {
        this.settings.loading = true;
        this.api.sendWithdrawMail({}).subscribe( res => {
            this.settings.loading = false;
            if (res.success) {
                this.showAuthModal = true;
                this.notify.showNotification('Confirm Email was send successfully. Please check your email and input code.', 'top', 'center', 'success');
            }
        }, err => {
            this.settings.loading = false;
        });
    }
    submitCode() {
        if (this.authCode == '') {
            return;
        }
        this.codeMatch = true;
        this.api.confirmWithdrawCode({
            code: this.authCode
        }).subscribe( res => {
            if (res.success) {
                this.withdraw(this.withdrawCoin);
                this.showAuthModal = false;
            } else {
                this.codeMatch = false;
            }
        }, err => {
            this.codeMatch = false;
        });
    }
}
