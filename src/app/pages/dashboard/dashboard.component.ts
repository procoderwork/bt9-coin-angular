import {Component, OnInit, AfterViewInit, OnDestroy} from '@angular/core';
import {TableData} from '../../md/md-table/md-table.component';
import {LegendItem, ChartType} from '../../md/md-chart/md-chart.component';

import * as Chartist from 'chartist';
import {Api} from "../../services/api.service";
import {SettingsService} from "../../services/settings/settings.service";
import {BalanceService} from "../../services/balance.service";
import {Router} from "@angular/router";

declare const $: any;
declare var swal: any;

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, AfterViewInit, OnDestroy {
    // constructor(private navbarTitleService: NavbarTitleService, private notificationService: NotificationService) { }
    public receivedTxList: any = [];

    btc_usd = 0;
    usd_btc = 0;

    eth_usd = 0;
    usd_eth = 0;

    usd_token = 0;
    token_usd = 0;

    token_btc = 0;
    token_eth = 0;

    tokenAmount = 0;

    countTime = 0;
    counttime_min = 0;

    sysSettings: any = {};
    remain:any = {};

    totalSold = 0;

    curStep = 0;
    curBonus = 0;

    tokenRates: any = [];

    agendas: any = [];

    timerID: any;
    startAnimationForLineChart(chart: any) {
    }

    startAnimationForBarChart(chart: any) {
    }

    constructor(
        private api: Api,
        public settings: SettingsService,
        public balance: BalanceService,
        public router: Router
    ) { }

    public ngOnInit() {
        if (this.settings.getUserSetting('role') == 'admin') {
            this.router.navigate(['/admin/dashboard']);
        }

        this.api.getICOAgenda({}).subscribe( res => {
            if (res.success) {
                this.agendas = res.data;
                var now = new Date();
                for (var i = 0; i < res.data.length; i ++) {
                    var from = new Date(res.data[i].from);
                    var to = new Date(res.data[i].to);
                    to.setDate(to.getDate() + 1);

                    if (from <= now && to > now) {

                        // this.curBonus = Number(this.tokenRates[this.curStep]['bonus']);
                        this.token_usd = Number(res.data[i].price);
                        this.usd_token = 1 / this.token_usd;
                    }
                }
                if (this.token_usd == 0) {
                    this.token_usd = Number(res.data[res.data.length - 1].price);
                    this.usd_token = 1 / this.token_usd;
                }
            }
        }, err => {

        });

        this.api.getSettings().subscribe( res => {
            if (res.success) {
                this.sysSettings = res.data;

                // this.api.getTotalSold({}).subscribe( res => {
                //     this.totalSold = res.balance;
                //     this.curStep = Math.floor(this.totalSold / 1000000);
                //
                //     // this.api.getTokenRate({}).subscribe( res => {
                //     //     if (res.success) {
                //     //
                //     //         this.tokenRates = res.data;
                //     //         this.curBonus = Number(this.tokenRates[this.curStep]['bonus']);
                //     //         this.token_usd = Number(this.tokenRates[this.curStep]['rate']);
                //     //         this.usd_token = 1 / this.token_usd;
                //     //     }
                //     // }, err => {
                //     //
                //     // });
                //     this.api.getICOAgenda({}).subscribe( res => {
                //         if (res.success) {
                //             this.agendas = res.data;
                //             var now = new Date();
                //             for (var i = 0; i < res.data.length; i ++) {
                //                 if (new Date(res.data[i].from) <= now && new Date(res.data[i].to) >= now) {
                //                     // this.curBonus = Number(this.tokenRates[this.curStep]['bonus']);
                //                     this.token_usd = Number(res.data[i].price);
                //                     this.usd_token = 1 / this.token_usd;
                //                 }
                //             }
                //             if (this.token_usd == 0) {
                //                 this.token_usd = Number(res.data[res.data.length - 1].price);
                //                 this.usd_token = 1 / this.token_usd;
                //             }
                //         }
                //     }, err => {
                //
                //     });
                // });
                //
                // var _parent = this;
                // setInterval(function() {
                //     var now:any = new Date();
                //     var ico_end:any = new Date(_parent.sysSettings.ico_end_datetime);
                //     var secs = Math.round((ico_end - now) / 1000);
                //
                //     _parent.remain.day = Math.floor(secs / (60 * 60 * 24));
                //     _parent.remain.hr = Math.floor((secs % (60 * 60 * 24)) / (60 * 60));
                //     _parent.remain.min = Math.floor(((secs % (60 * 60 * 24)) % (60 * 60)) / 60);
                //     _parent.remain.sec = secs % 60;
                // }, 1000);
            }
        });

        this.loadCoinRate();
        // this.loadTokenRate();
        // this.loadICOAgenda();

        let _parent = this;
        //
        this.timerID = setInterval(function() {
            _parent.countTime --;
            if (_parent.countTime <= 0) {
                _parent.countTime = 119;
                _parent.loadCoinRate();
            }

            _parent.counttime_min = Math.floor(_parent.countTime / 60);
        }, 1000);
    }

    ngAfterViewInit() {

    }

    ngOnDestroy() {
        if (this.timerID) {
            clearInterval(this.timerID);
            console.log('timer clear');
        }
    }

    loadICOAgenda() {
        this.api.getICOAgenda({}).subscribe( res => {
            if (res.success) {
                this.agendas = res.data;
            }
        });
    }

    loadCoinRate() {
        this.api.getCoinRate({}).subscribe( res => {
            if (res.success) {
                this.btc_usd = res.btc_rate;
                this.usd_btc = 1 / Number(res.btc_rate);

                this.eth_usd = res.eth_rate;
                this.usd_eth = 1 / Number(res.eth_rate);

                this.token_btc = this.token_usd * this.usd_btc;
                this.token_eth = this.token_usd * this.usd_eth;
            }
        }, err => {

        });
    }

    loadTokenRate() {
        this.api.getTokenRate({}).subscribe( res => {
            if (res.success) {
                this.token_usd = res.rate;
                this.usd_token = 1 / Number(res.rate);

                this.token_btc = this.token_usd * this.usd_btc;
                this.token_eth = this.token_usd * this.usd_eth;
            }
        }, err => {

        });
    }

    getBTCAmount() {
        return this.tokenAmount * this.token_btc;
    }
    getETHAmount() {
        return this.tokenAmount * this.token_eth;
    }

    allBTC() {
        var btcBalance = this.settings.getUserSetting('btc_balance');

        this.tokenAmount = btcBalance * (1 / this.token_btc);
    }

    allETH() {
        var ethBalance = this.settings.getUserSetting('eth_balance');

        this.tokenAmount = ethBalance * (1 / this.token_eth);
    }

    buyTokenWithBTC() {
        if (Number(this.tokenAmount) == 0) {
            return;
        }
        if (Number(this.tokenAmount) < 5) {
            swal({
                type: 'warning',
                title: 'Warning',
                text: 'Minimum Token Amount is 5',
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-success'
            });
            return;
        }

        if (Number(this.tokenAmount * this.token_btc) > Number(this.settings.getUserSetting('btc_balance'))) {
            swal({
                type: 'warning',
                title: 'Warning',
                text: 'Your Bitcoin balance is not able',
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-success'
            });
            return;
        }

        this.api.buyTokenWithBTC({
            amount: this.tokenAmount,
            token_btc: this.token_btc,
            // bonus: this.tokenAmount * this.curBonus / 100,
            token_usd: this.token_usd
        }).subscribe( res => {
            if (res.success) {
                swal({
                    type: 'success',
                    title: 'Success',
                    text: 'You have bought BT9Coin with BTC successfully',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-success'
                });

                this.balance.getTokenBalance();
                this.balance.getBtcBalance();
            } else {
                swal({
                    type: 'warning',
                    title: 'Failed',
                    text: res.error,
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-success'
                });
            }
        }, err => {
            swal({
                type: 'warning',
                title: 'Failed',
                text: err._body,
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-success'
            });
        });

    }

    buyTokenWithETH() {
        if (Number(this.tokenAmount) == 0) {
            return;
        }
        if (Number(this.tokenAmount) < 5) {
            swal({
                type: 'warning',
                title: 'Warning',
                text: 'Minimum MyAltCoin Amount is 5',
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-success'
            });
            return;
        }
        if (Number(this.tokenAmount * this.token_eth) > Number(this.settings.getUserSetting('eth_balance'))) {
            swal({
                type: 'warning',
                title: 'Warning',
                text: 'Your Ethereum balance is not able',
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-success'
            });
            return;
        }

        this.api.buyTokenWithETH({
            amount: this.tokenAmount,
            token_eth: this.token_eth,
            bonus: this.tokenAmount * this.curBonus / 100,
            token_usd: this.token_usd
        }).subscribe( res => {
            if (res.success) {
                swal({
                    type: 'success',
                    title: 'Success',
                    text: 'You have bought BT9Coin with ETH successfully',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-success'
                });

                this.balance.getTokenBalance();
                this.balance.getEthBalance();
            } else {
                swal({
                    type: 'warning',
                    title: 'Failed',
                    text: res.error,
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-success'
                });
            }
        }, err => {
            swal({
                type: 'warning',
                title: 'Failed',
                text: err._body,
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-success'
            });
        });

    }
}
