import {Component, OnInit, AfterViewInit} from '@angular/core';
import * as Chartist from 'chartist';
import {Notifications} from "../../services/notifications.service";
import {SettingsService} from "../../services/settings/settings.service";
import {Api} from "../../services/api.service";
import {BalanceService} from "../../services/balance.service";

declare const $: any;
declare var swal: any;

@Component({
    selector: 'app-staking',
    templateUrl: './staking.component.html',
    styleUrls: ['./staking.component.scss']
})
export class StakingComponent implements OnInit {
    stakingAmount = 0;
    stakings: any = [];

    stakingBalance: any = {};
    dailyBalance: any = {};

    constructor(
        public settings: SettingsService,
        public api: Api,
        public balance: BalanceService,
        public notify: Notifications
    ) {
    }

    ngOnInit() {
        this.loadStakings();
        this.loadStakingBalance();
    }

    loadStakings() {
        this.api.getStaking({}).subscribe( res => {
            if (res.success) {
                this.stakings = res.data;
            }
        }, err => {

        });
    }

    loadStakingBalance() {
        this.api.getStakingBalance({}).subscribe( res => {
            if (res.success) {
                this.stakingBalance.total_amount = res.total_amount;
                this.stakingBalance.locked_amount = res.locked_amount;
                this.stakingBalance.unlocked_amount = res.total_amount - res.locked_amount;
                this.stakingBalance.earned_amount = res.earned_amount;
            }
        }, err => {

        });
    }
    buyStaking() {
        if (Number(this.stakingAmount) < this.settings.getSysSetting('staking_minimum_amount')) {
            swal({
                type: 'warning',
                title: 'Warning',
                text: 'Minimum Staking Amount is ' + this.settings.getSysSetting('staking_minimum_amount'),
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-success'
            });
            return;
        }

        this.settings.loading = true;
        this.api.saveStaking({
            amount: this.stakingAmount,
            after: this.settings.getSysSetting('staking_locked_days'),
            daily: this.settings.getSysSetting('staking_interest_month') / 30
        }).subscribe( res => {
            this.settings.loading = false;
            if (res.success) {
                swal({
                    type: 'success',
                    title: 'Success',
                    text: 'Successfully',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-success'
                });

                this.balance.getTokenBalance();
                this.loadStakings();
                this.loadStakingBalance();
            } else {
                swal({
                    type: 'warning',
                    title: 'Failed',
                    text: res.error,
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-success'
                });
            }
        }, err => {
            this.settings.loading = false;
            swal({
                type: 'warning',
                title: 'Failed',
                text: err._body,
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-success'
            });
        });
    }

    allBT9() {
        this.stakingAmount = this.settings.getUserSetting('token_balance');
    }
}
