import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MdModule } from '../../md/md.module';
import { MaterialModule } from '../../app.module';

import {StakingComponent} from './staking.component';
import {StakingRoutes} from './staking.routing';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(StakingRoutes),
        FormsModule,
        MdModule,
        MaterialModule
    ],
    declarations: [StakingComponent]
})

export class StakingModule {}
