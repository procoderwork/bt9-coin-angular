import { Routes } from '@angular/router';

import {StakingComponent} from './staking.component';

export const StakingRoutes: Routes = [
    {
        path: '',
        component: StakingComponent
    }
];
