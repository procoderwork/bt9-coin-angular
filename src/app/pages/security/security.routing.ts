import { Routes } from '@angular/router';

import { SecurityComponent } from './security.component';

export const SecurityRoutes: Routes = [
    {
        path: '',
        component: SecurityComponent
    }
];
