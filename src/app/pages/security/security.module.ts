import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MdModule} from '../../md/md.module';
import {MaterialModule} from '../../app.module';

import {SecurityComponent} from './security.component';
import {SecurityRoutes} from './security.routing';
import {SharedModule} from "../../shared/shared.module";

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(SecurityRoutes),
        FormsModule,
        MdModule,
        MaterialModule,
        ReactiveFormsModule,
        SharedModule
    ],
    declarations: [SecurityComponent]
})

export class SecurityModule {
}
