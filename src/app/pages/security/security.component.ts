import {Component, OnInit, AfterViewInit, Input} from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormControl, AbstractControl} from '@angular/forms';
import {TableData} from '../../md/md-table/md-table.component';
import {LegendItem, ChartType} from '../../md/md-chart/md-chart.component';

import * as Chartist from 'chartist';
import {SettingsService} from "../../services/settings/settings.service";
import {Api} from "../../services/api.service";
import {Notifications} from "../../services/notifications.service";
import {Validate} from "../../services/validate.service";
import {PasswordValidation} from "../../shared/components/password-validator.component";

declare const $: any;
declare var swal: any;

@Component({
    selector: 'app-security',
    templateUrl: './security.component.html',
    styleUrls: ['./security.component.scss']
})
export class SecurityComponent implements OnInit {
    changepwd: FormGroup;
    pwdInfo: any = {};

    authCode = '';
    codeMatch = true;
    showAuthModal = false;

    allow_g2f = 0;
    g2f_code = '';
    email = '';

    value = 0;

    loginHistory: any = [];
    constructor(
        private formBuilder: FormBuilder,
        public settings: SettingsService,
        public api: Api,
        public notify: Notifications,
        public validate: Validate
    ) {}

    ngOnInit() {
        this.email = this.settings.getUserSetting('email');
        this.allow_g2f = this.settings.getUserSetting('allow_g2f');
        console.log(this.allow_g2f);

        if (this.settings.getUserSetting('g2f_code') == null || this.settings.getUserSetting('g2f_code') == '') {
            this.loadG2FCode();
        } else {
            this.g2f_code = this.settings.getUserSetting('g2f_code');
        }

        this.api.getLoginHistory({}).subscribe(res => {
            if (res.success) {
                this.loginHistory = res.data;

                let _parent = this;
                var timerID = setInterval(function() {
                    if ($('#listLoginHistory tbody tr').length == res.data.length) {
                        clearInterval(timerID);
                        _parent.buildLoginHistoryTable();
                    }
                }, 200);
            }
        }, err => {

        });
        this.changepwd = this.formBuilder.group({
            // To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
            // We can use more than one validator per field. If we want to use more than one validator we have to wrap our array of validators with a Validators.compose function. Here we are using a required, minimum length and maximum length validator.
            password: ['', Validators.required],
            confirmPassword: ['', Validators.required],
            oldPassword: ['', Validators.required],
        }, {
            validator: PasswordValidation.MatchPassword // your validation method
        });
    }

    buildLoginHistoryTable() {
        $('#listLoginHistory').DataTable({
            'pagingType': 'full_numbers',
            'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
            responsive: true,
            language: {
                search: '_INPUT_',
                searchPlaceholder: 'Search records',
            }
        });
    }

    loadG2FCode() {
        this.api.getG2FCode({}).subscribe( res => {
            if (res.success) {
                this.g2f_code = res.code;
                this.settings.setUserSetting('g2f_code', res.code);
            }
        }, err => {

        });
    }
    enableG2F() {
        if ($('#verify_auth_key').prop('checked') == false) {
            $('#required').show();
            return;
        }
        this.value = 1;
        this.showAuthModal = true;
    }

    disableG2F() {
        this.value = 0;
        this.showAuthModal = true;
    }

    submitCode() {
        if (this.authCode == '') {
            return;
        }
        this.codeMatch = true;
        this.api.confirmG2FCode({
            code: this.authCode
        }).subscribe( res => {
            if (res.success) {

                this.api.setG2F({value: this.value}).subscribe( res => {
                    this.allow_g2f = this.value;
                    this.settings.setUserSetting('allow_g2f', this.value);

                    if (this.value == 0) {
                        this.loadG2FCode();
                    }
                }, err => {

                });
                this.showAuthModal = false;
            } else {
                this.codeMatch = false;
            }
        }, err => {
            this.codeMatch = false;
        });
    }

    changePwd() {
        if (this.changepwd.valid) {
            this.api.chagePwd(this.pwdInfo).subscribe(res => {
                if (res.success) {
                    this.notify.showNotification('Password has changed', 'top', 'center', 'success');
                } else {
                    this.notify.showNotification(res.error, 'top', 'center', 'warning');
                }
            }, err => {
                this.notify.showNotification('Error', 'top', 'center', 'danger');
            });
        } else {
            this.validate.validateAllFormFields(this.changepwd);
        }
    }

    changeLoginMsg() {
        this.api.changeUserInfo({
            status: (Number(this.settings.getUserSetting('enable_loginmsg')) + 1) % 2
        }).subscribe(res => {
            if (res.success) {
                this.settings.setUserSetting('enable_loginmsg', (Number(this.settings.getUserSetting('enable_loginmsg')) + 1) % 2);
            }
        });
    }

    changeWithdrawMail() {
        this.api.changeWithdrawMailStatus({
            status: (Number(this.settings.getUserSetting('enable_withdrawmail')) + 1) % 2
        }).subscribe(res => {
            if (res.success) {
                this.settings.setUserSetting('enable_withdrawmail', (Number(this.settings.getUserSetting('enable_withdrawmail')) + 1) % 2);
            }
        });
    }

    isMobileMenu() {
        if ($(window).width() < 991) {
            return false;
        }
        return true;
    }
}
