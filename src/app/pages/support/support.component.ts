import {Component, OnInit, AfterViewInit, ViewEncapsulation} from '@angular/core';
import {Api} from "../../services/api.service";
import {SettingsService} from "../../services/settings/settings.service";
import {Notifications} from "../../services/notifications.service";

declare const $: any;

@Component({
    selector: 'app-support',
    templateUrl: './support.component.html',
    styleUrls: ['./support.component.scss']
})
export class SupportComponent implements OnInit {tickets: any = [];
    details: any = [];

    isNew: boolean = false;
    isReply: boolean = false;

    ticket: any = {};

    constructor(
        public api: Api,
        public settings: SettingsService,
        public notify: Notifications
    ) {}

    ngOnInit() {
        this.loadMyTicket();
    }

    loadMyTicket() {
        this.api.getMyTicket({}).subscribe( res=> {
            if (res.success) {
                this.tickets = res.data;
            }
        });
    }

    submitTicket() {
        this.isNew = true;
        this.isReply = false;
        this.ticket = {};
    }

    onReply() {
        this.ticket.message = '';
        this.ticket.attach_url = '';
        this.ticket.attach_name = '';

        this.isNew = false;
        this.isReply = true;
    }

    showTicket(ticket) {
        this.ticket = ticket;
        this.isNew = false;
        this.isReply = false;

        this.settings.loading = true;
        this.api.getTicketDetail({
            ticket_id: ticket.id
        }).subscribe( res=> {
            this.settings.loading = false;
            if (res.success) {
                this.details = res.data;
            } else {
                this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        }, err=> {
            this.settings.loading = false;
            this.notify.showNotification('You can not see detail.', 'top', 'center', 'error');
        });
    }

    sendTicket() {
        if (this.isNew) {
            this.settings.loading = true;
            this.api.postTicket(this.ticket).subscribe( res => {
                this.settings.loading = false;
                if (res.success) {
                    this.notify.showNotification('Your Ticket was posted successfully', 'top', 'center', 'success');
                    this.loadMyTicket();

                    this.isNew = false;
                    this.ticket = {};
                } else {
                    this.notify.showNotification(res.error, 'top', 'center', 'warning');
                }
            }, err => {
                this.settings.loading = false;
                this.notify.showNotification('Please try again.', 'top', 'center', 'danger');
            });
        }
        else if (this.isReply) {
            this.settings.loading = true;
            this.api.replyTicket(this.ticket).subscribe( res => {
                this.settings.loading = false;
                if (res.success) {
                    this.notify.showNotification('Your Reply Ticket was posted successfully',  'top', 'center', 'success');

                    this.isReply = false;
                    this.showTicket(this.ticket);
                } else {
                    this.notify.showNotification(res.error,  'top', 'center', 'warning');
                }
            }, err => {
                this.settings.loading = false;
                this.notify.showNotification('Please try again.',  'top', 'center', 'danger');
            });
        }
    }

    onAttachFile() {
        $('#attachFile').click();
    }

    fileChangeListener($event) {
        let image: any = new Image();
        let file: File = $event.target.files[0];

        this.settings.loading = true;
        this.api.uploadFile(file).subscribe(res => {
            this.settings.loading = false;
            if (res.success) {
                this.ticket.attach_url = res.url;
                this.ticket.attach_name = file.name;
            }
        }, err => {
            this.settings.loading = false;
        });
    }
}
