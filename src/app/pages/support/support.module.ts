import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MdModule } from '../../md/md.module';
import { MaterialModule } from '../../app.module';

import {SupportComponent} from './support.component';
import {SupportRoutes} from './support.routing';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(SupportRoutes),
        FormsModule,
        MdModule,
        MaterialModule,
    ],
    declarations: [
        SupportComponent
    ]
})

export class SupportModule {}
