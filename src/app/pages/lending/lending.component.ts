import {Component, OnInit, AfterViewInit} from '@angular/core';
import {TableData} from '../../md/md-table/md-table.component';
import {LegendItem, ChartType} from '../../md/md-chart/md-chart.component';

import * as Chartist from 'chartist';
import {SettingsService} from "../../services/settings/settings.service";
import {Api} from "../../services/api.service";
import {BalanceService} from "../../services/balance.service";
import {Notifications} from "../../services/notifications.service";

declare const $: any;
declare var swal: any;

@Component({
    selector: 'app-lending',
    templateUrl: './lending.component.html',
    styleUrls: ['./lending.component.scss']
})
export class LendingComponent implements OnInit {
    usdAmount = 0;
    lendingSettings: any = [];
    lendings: any = [];

    interests: any = [];

    lendingBalance: any = {};
    dailyBalance: any = {};

    token_usd = 0;
    usd_token = 0;

    transfer_amount = 0;
    showTransferModal = false;

    type = '';

    constructor(
        public settings: SettingsService,
        public api: Api,
        public balance: BalanceService,
        public notify: Notifications
    ) {
    }

    ngOnInit() {
        this.api.getLendingSetting({}).subscribe( res => {
            if (res.success) {
                this.lendingSettings = res.data;
            }
        }, err => {

        });

        this.api.getTokenRate({}).subscribe( res => {
            if (res.success) {
                this.token_usd = res.rate;
                this.usd_token = 1 / this.token_usd;
            }
        }, err => {

        });

        this.api.getInterestDaily({}).subscribe( res => {
            if (res.success) {
                this.interests = res.data;

                this.loadDailyBalance();
            }
        });

        this.loadLending();
        this.loadLendingBalance();
    }

    loadLending() {
        this.api.getLending({}).subscribe( res => {
            if (res.success) {
                this.lendings = res.data;
            }
        }, err => {

        });
    }

    loadLendingBalance() {
        this.api.getLendingBalance({}).subscribe( res => {
            if (res.success) {
                this.lendingBalance.locked_amount = res.locked_amount;
                this.lendingBalance.unlocked_amount = res.unlocked_amount;
                this.lendingBalance.transfer_amount = res.transfer_amount;
                this.lendingBalance.balance = res.unlocked_amount - res.transfer_amount;
            }
        }, err => {

        });
    }

    loadDailyBalance() {
        this.api.getLendingDaily({}).subscribe( res => {
            if (res.success) {
                this.dailyBalance.amount = res.amount;
                this.dailyBalance.transfer_amount = res.transfer_amount;
                this.dailyBalance.balance = res.amount - res.transfer_amount;
            }
        }, err => {

        });
    }

    allBT9() {
        this.usdAmount = this.settings.getUserSetting('token_balance') * this.token_usd;
    }

    getDaily() {
        var result = 0;
        for (var i = 0; i < this.lendingSettings.length; i ++) {
            if (this.lendingSettings[i].min <= this.usdAmount && this.lendingSettings[i].max >= this.usdAmount) {
                return this.lendingSettings[i].daily;
            }
        }
        return 0;
    }

    getAfter() {
        var result = 0;
        for (var i = 0; i < this.lendingSettings.length; i ++) {
            if (this.lendingSettings[i].min <= this.usdAmount && this.lendingSettings[i].max >= this.usdAmount) {
                return this.lendingSettings[i].after;
            }
        }
        return 0;
    }

    buyLending() {
        if (Number(this.usdAmount) < 100) {
            swal({
                type: 'warning',
                title: 'Warning',
                text: 'Minimum Lending Amount is 100',
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-success'
            });
            return;
        }

        if (Number(this.usdAmount) % 1 > 0) {
            swal({
                type: 'warning',
                title: 'Warning',
                text: 'Lending Amount must be integer',
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-success'
            });
            return;
        }

        this.settings.loading = true;
        this.api.saveLending({
            amount: this.usdAmount,
            after: this.getAfter(),
            token_amount: this.usdAmount * this.usd_token,
            daily: this.getDaily()
        }).subscribe( res => {
            this.settings.loading = false;
            if (res.success) {
                swal({
                    type: 'success',
                    title: 'Success',
                    text: 'Successfully',
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-success'
                });

                this.balance.getUsdBalance();
                this.balance.getTokenBalance();
                this.loadLending();
                this.loadLendingBalance();
                this.loadDailyBalance();
            } else {
                swal({
                    type: 'warning',
                    title: 'Failed',
                    text: res.error,
                    buttonsStyling: false,
                    confirmButtonClass: 'btn btn-success'
                });
            }
        }, err => {
            this.settings.loading = false;
            swal({
                type: 'warning',
                title: 'Failed',
                text: err._body,
                buttonsStyling: false,
                confirmButtonClass: 'btn btn-success'
            });
        });
    }
    transferDaily() {
        if (this.dailyBalance.balance <= 0) {
            this.notify.showNotification('Your balance is 0', 'top', 'center', 'warning');
            return;
        }
        this.transfer_amount = this.dailyBalance.balance;
        this.type = 'daily';
        this.showTransferModal = true;
    }
    transferLending() {
        if (this.lendingBalance.balance <= 0) {
            this.notify.showNotification('Your balance is 0', 'top', 'center', 'warning');
            return;
        }
        this.transfer_amount = this.lendingBalance.balance;
        this.type = 'lending';
        this.showTransferModal = true;
    }
    submitTransfer() {
        if (this.type == 'lending') {
            this.api.transferLending({
                amount: this.transfer_amount,
                token_amount: this.transfer_amount * this.usd_token,
                rate: this.usd_token
            }).subscribe( res => {
                if (res.success) {
                    this.notify.showNotification('Successfully transfered', 'top', 'center', 'success');
                    this.showTransferModal = false;
                    this.loadLendingBalance();
                    this.balance.getTokenBalance();
                } else {
                    this.notify.showNotification(res.error, 'top', 'center', 'warning');
                }
            });
        } else {
            this.api.transferDailyLending({
                amount: this.transfer_amount,
                token_amount: this.transfer_amount * this.usd_token,
                rate: this.usd_token
            }).subscribe( res => {
                if (res.success) {
                    this.notify.showNotification('Successfully transfered', 'top', 'center', 'success');
                    this.showTransferModal = false;
                    this.loadDailyBalance();
                    this.balance.getTokenBalance();
                } else {
                    this.notify.showNotification(res.error, 'top', 'center', 'warning');
                }
            });
        }
    }
}
