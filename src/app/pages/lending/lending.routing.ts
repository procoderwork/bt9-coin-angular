import { Routes } from '@angular/router';

import {LendingComponent} from './lending.component';

export const LendingRoutes: Routes = [
    {
        path: '',
        component: LendingComponent
    }
];
