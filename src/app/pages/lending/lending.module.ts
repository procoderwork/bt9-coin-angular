import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MdModule } from '../../md/md.module';
import { MaterialModule } from '../../app.module';

import {LendingComponent} from './lending.component';
import {LendingRoutes} from './lending.routing';
import {SharedModule} from "../../shared/shared.module";

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(LendingRoutes),
        FormsModule,
        MdModule,
        MaterialModule,
        SharedModule.forRoot()
    ],
    declarations: [LendingComponent]
})

export class LendingModule {}
