import {Component, OnInit, AfterViewInit, ViewEncapsulation} from '@angular/core';
import {TableData} from '../../md/md-table/md-table.component';
import {LegendItem, ChartType} from '../../md/md-chart/md-chart.component';

import * as Chartist from 'chartist';
import {Api} from "../../services/api.service";

declare const $: any;

@Component({
    selector: 'app-stareferralking',
    templateUrl: './referral.component.html',
    encapsulation: ViewEncapsulation.None
})
export class ReferralComponent implements OnInit {
    referraldata: any = [];

    constructor(
        public api: Api
    ) {}

    ngOnInit() {
        this.api.getReferral({}).subscribe( res => {
            if (res.success) {
                this.referraldata = res.data;
            }
        }, err => {

        });
    }
}
