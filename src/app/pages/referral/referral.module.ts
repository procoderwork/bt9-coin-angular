import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MdModule } from '../../md/md.module';
import { MaterialModule } from '../../app.module';

import {ReferralComponent} from './referral.component';
import {ReferralRoutes} from './referral.routing';
import {TreeTableModule} from "ng-treetable";

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ReferralRoutes),
        FormsModule,
        MdModule,
        MaterialModule,
        TreeTableModule
    ],
    declarations: [
        ReferralComponent
    ],
    bootstrap: [
        ReferralComponent
    ]
})

export class ReferralModule {}
