import { Routes } from '@angular/router';

import {ReferralComponent} from './referral.component';

export const ReferralRoutes: Routes = [
    {
        path: '',
        component: ReferralComponent
    }
];
