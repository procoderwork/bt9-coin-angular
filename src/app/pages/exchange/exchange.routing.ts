import { Routes } from '@angular/router';

import {ExchangeBTCComponent} from './btc/exchangebtc.component';
import {ExchangeETHComponent} from "./eth/exchangeeth.component";

export const ExchangeRoutes: Routes = [
    {
        path: '',
        component: ExchangeBTCComponent
    },
    {
        path: 'btc',
        component: ExchangeBTCComponent
    },
    {
        path: 'eth',
        component: ExchangeETHComponent
    }
];
