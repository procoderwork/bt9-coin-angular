import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MdModule } from '../../md/md.module';
import { MaterialModule } from '../../app.module';

import {ExchangeBTCComponent} from './btc/exchangebtc.component';
import {ExchangeRoutes} from './exchange.routing';
import {SharedModule} from "../../shared/shared.module";
import {ExchangeETHComponent} from "./eth/exchangeeth.component";
import {ChartModule, HIGHCHARTS_MODULES} from "angular-highcharts";


import more from 'highcharts/highcharts-more.src';
import exporting from 'highcharts/modules/exporting.src';
import highstock from 'highcharts/modules/stock.src';

export function highchartsModules() {
    return [more, exporting, highstock];
}

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ExchangeRoutes),
        FormsModule,
        MdModule,
        MaterialModule,
        SharedModule,
        ChartModule
    ],
    declarations: [
        ExchangeBTCComponent,
        ExchangeETHComponent
    ],
    providers: [
        {
            provide: HIGHCHARTS_MODULES,
            useFactory: highchartsModules
        }
    ]
})

export class ExchangeModule {}
