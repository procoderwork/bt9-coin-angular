import {Component, OnInit, AfterViewInit} from '@angular/core';
import {TableData} from '../../../md/md-table/md-table.component';
import {LegendItem, ChartType} from '../../../md/md-chart/md-chart.component';

import * as Chartist from 'chartist';
import {Api} from "../../../services/api.service";
import {SettingsService} from "../../../services/settings/settings.service";
import {BalanceService} from "../../../services/balance.service";
import {Notifications} from "../../../services/notifications.service";
import {Chart, StockChart, Highcharts} from 'angular-highcharts';

declare const swal: any;
declare const $: any;

@Component({
    selector: 'app-exchange',
    templateUrl: './exchangebtc.component.html',
    styleUrls: ['./exchangebtc.component.scss']
})
export class ExchangeBTCComponent implements OnInit {
    icoChartData: any = [];

    buy: any = {};
    sell: any = {};

    buyOrders: any = [];
    sellOrders: any = [];

    openOrders: any = [];
    pastOrders: any = [];

    buyloading = false;
    sellloading = false;

    stockChart: any;
    exchangeInfo: any = {};

    btc_usd = 0;
    usd_btc = 0;
    constructor(public api: Api,
                public settings: SettingsService,
                public balance: BalanceService,
                public notify: Notifications) {
    }

    startAnimationForLineChart(chart: any) {
        let seq: number, delays: number, durations: number;
        seq = 0;
        delays = 80;
        durations = 500;
        chart.on('draw', function (data: any) {

            if (data.type === 'line' || data.type === 'area') {
                data.element.animate({
                    d: {
                        begin: 600,
                        dur: 700,
                        from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
                        to: data.path.clone().stringify(),
                        easing: Chartist.Svg.Easing.easeOutQuint
                    }
                });
            } else if (data.type === 'point') {
                seq++;
                data.element.animate({
                    opacity: {
                        begin: seq * delays,
                        dur: durations,
                        from: 0,
                        to: 1,
                        easing: 'ease'
                    }
                });
            }
        });

        seq = 0;
    }

    ngOnInit() {
        this.getBuyOrders();
        this.getSellOrders();
        this.getPastOrders();
        this.getOpenOrders();

        this.api.getCoinRate({}).subscribe( res => {
            if (res.success) {
                this.btc_usd = res.btc_rate;
                this.usd_btc = 1 / Number(res.btc_rate);
            }
        }, err => {

        });

        // this.stockChart = new StockChart({});

        this.api.getExchangeStatistics('BTC', {}).subscribe( res => {
            if (res.success) {
                this.exchangeInfo = res.data;

                if (this.exchangeInfo.lastPrice > 0) {
                    this.buy.rate = Number(this.exchangeInfo.lastPrice);
                    this.sell.rate = Number(this.exchangeInfo.lastPrice);
                }
            }
        });

        var _parent = this;
        this.api.getExchangeHistory('BTC', {}).subscribe(res => {
            if (res.success) {
                // split the data set into ohlc and volume
                var ohlc = [],
                    volume = [],
                    dataLength = res.data.length,
                    // set the allowed units for data grouping
                    groupingUnits = [[
                        'minute',                         // unit name
                        [1, 2, 3, 4, 6]                             // allowed multiples
                    ], [
                        'hour',
                        [1, 2, 3, 4, 5, 6, 7]
                    ]],

                    i = 0;

                for (i; i < res.data.length; i += 1) {
                    ohlc.push([
                        Number(res.data[i].date), // the date
                        Number(res.data[i].open), // open
                        Number(res.data[i].high), // high
                        Number(res.data[i].low), // low
                        Number(res.data[i].close) // close
                    ]);

                    volume.push([
                        Number(res.data[i].date), // the date
                        Number(res.data[i].volume) // the volume
                    ]);
                }
                // create the chart
                _parent.stockChart = new StockChart({
                    rangeSelector: {
                        selected: 0,
                        buttons: [{
                            type: 'hour',
                            count: 6,
                            text: '6h',
                        }, {
                            type: 'hour',
                            count: 12,
                            text: '12h'
                        }, {
                            type: 'hour',
                            count: 24,
                            text: '24h'
                        }, {
                            type: 'day',
                            count: 3,
                            text: '3d'
                        }, {
                            type: 'day',
                            count: 7,
                            text: '1w'
                        }, {
                            type: 'all',
                            text: 'All'
                        }]
                    },

                    title: {
                        text: 'Market History'
                    },

                    yAxis: [{
                        labels: {
                            align: 'right',
                            x: -3
                        },
                        title: {
                            text: 'Price'
                        },
                        height: '60%',
                        lineWidth: 2,
                        resize: {
                            enabled: true
                        }
                    }, {
                        labels: {
                            align: 'right',
                            x: -3
                        },
                        title: {
                            text: 'Volume'
                        },
                        top: '65%',
                        height: '35%',
                        offset: 0,
                        lineWidth: 2
                    }],

                    tooltip: {
                        split: true
                    },

                    series: [{
                        type: 'candlestick',
                        name: 'BTC Exchange Price',
                        data: ohlc,
                        dataGrouping: {
                            units: groupingUnits
                        }
                    }, {
                        type: 'column',
                        name: 'Volume(BTC)',
                        data: volume,
                        yAxis: 1,
                        dataGrouping: {
                            units: groupingUnits
                        }
                    }]
                });
            }
        });
        // var theme = {
        //     //    colors: ['#2b908f', '#90ee7e', '#f45b5b', '#7798BF', '#aaeeee', '#ff0066', '#eeaaee',
        //     //       '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
        //     chart: {
        //         backgroundColor: {
        //             linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
        //             stops: [
        //                 [0, '#2a2a2b'],
        //                 [1, '#3e3e40']
        //             ]
        //         },
        //         style: {
        //             fontFamily: '\'Unica One\', sans-serif'
        //         },
        //         plotBorderColor: '#606063'
        //     },
        //     title: {
        //         style: {
        //             color: '#E0E0E3',
        //             textTransform: 'uppercase',
        //             fontSize: '20px'
        //         }
        //     },
        //     subtitle: {
        //         style: {
        //             color: '#E0E0E3',
        //             textTransform: 'uppercase'
        //         }
        //     },
        //     xAxis: {
        //         gridLineColor: '#707073',
        //         labels: {
        //             style: {
        //                 color: '#E0E0E3'
        //             }
        //         },
        //         lineColor: '#707073',
        //         minorGridLineColor: '#505053',
        //         tickColor: '#707073',
        //         title: {
        //             style: {
        //                 color: '#A0A0A3'
        //
        //             }
        //         }
        //     },
        //     yAxis: {
        //         gridLineColor: '#707073',
        //         labels: {
        //             style: {
        //                 color: '#E0E0E3'
        //             }
        //         },
        //         lineColor: '#707073',
        //         minorGridLineColor: '#505053',
        //         tickColor: '#707073',
        //         tickWidth: 1,
        //         title: {
        //             style: {
        //                 color: '#A0A0A3'
        //             }
        //         }
        //     },
        //     tooltip: {
        //         backgroundColor: 'rgba(0, 0, 0, 0.85)',
        //         style: {
        //             color: '#F0F0F0'
        //         }
        //     },
        //     plotOptions: {
        //         series: {
        //             dataLabels: {
        //                 color: '#B0B0B3'
        //             },
        //             marker: {
        //                 lineColor: '#333'
        //             }
        //         },
        //         boxplot: {
        //             fillColor: '#505053'
        //         },
        //         candlestick: {
        //             lineColor: 'white'
        //         },
        //         errorbar: {
        //             color: 'white'
        //         }
        //     },
        //     legend: {
        //         itemStyle: {
        //             color: '#E0E0E3'
        //         },
        //         itemHoverStyle: {
        //             color: '#FFF'
        //         },
        //         itemHiddenStyle: {
        //             color: '#606063'
        //         }
        //     },
        //     credits: {
        //         style: {
        //             color: '#666'
        //         }
        //     },
        //     labels: {
        //         style: {
        //             color: '#707073'
        //         }
        //     },
        //
        //     drilldown: {
        //         activeAxisLabelStyle: {
        //             color: '#F0F0F3'
        //         },
        //         activeDataLabelStyle: {
        //             color: '#F0F0F3'
        //         }
        //     },
        //
        //     navigation: {
        //         buttonOptions: {
        //             symbolStroke: '#DDDDDD',
        //             theme: {
        //                 fill: '#505053'
        //             }
        //         }
        //     },
        //
        //     // scroll charts
        //     rangeSelector: {
        //         buttonTheme: {
        //             fill: '#505053',
        //             stroke: '#000000',
        //             style: {
        //                 color: '#CCC'
        //             },
        //             states: {
        //                 hover: {
        //                     fill: '#707073',
        //                     stroke: '#000000',
        //                     style: {
        //                         color: 'white'
        //                     }
        //                 },
        //                 select: {
        //                     fill: '#000003',
        //                     stroke: '#000000',
        //                     style: {
        //                         color: 'white'
        //                     }
        //                 }
        //             }
        //         },
        //         inputBoxBorderColor: '#505053',
        //         inputStyle: {
        //             backgroundColor: '#333',
        //             color: 'silver'
        //         },
        //         labelStyle: {
        //             color: 'silver'
        //         }
        //     },
        //
        //     navigator: {
        //         handles: {
        //             backgroundColor: '#666',
        //             borderColor: '#AAA'
        //         },
        //         outlineColor: '#CCC',
        //         maskFill: 'rgba(255,255,255,0.1)',
        //         series: {
        //             color: '#7798BF',
        //             lineColor: '#A6C7ED'
        //         },
        //         xAxis: {
        //             gridLineColor: '#505053'
        //         }
        //     },
        //
        //     scrollbar: {
        //         barBackgroundColor: '#808083',
        //         barBorderColor: '#808083',
        //         buttonArrowColor: '#CCC',
        //         buttonBackgroundColor: '#606063',
        //         buttonBorderColor: '#606063',
        //         rifleColor: '#FFF',
        //         trackBackgroundColor: '#404043',
        //         trackBorderColor: '#404043'
        //     },
        //
        //     // special colors for some of the
        //     legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
        //     background2: '#505053',
        //     dataLabelsColor: '#B0B0B3',
        //     textColor: '#C0C0C0',
        //     contrastTextColor: '#F0F0F3',
        //     maskColor: 'rgba(255,255,255,0.3)'
        // };
        // Highcharts.setOptions(theme);
        // this.api.getICOAgendaChart({}).subscribe(res => {
        //     if (res.success) {
        //         this.icoChartData = res.data;
        //
        //         let labels: any = [];
        //         let series: any = [];
        //         let high = 0;
        //         let low = 100000;
        //
        //         let size = 0;
        //         for (var i = 0; i < res.data.length; i++) {
        //             let from = res.data[i].from;
        //             let to = res.data[i].to;
        //
        //             let date = new Date(from);
        //             while (date <= new Date(to)) {
        //                 labels[size ++] = (date.getMonth() + 1) + '/' + (date.getDate());
        //                 series[size ++] = res.data[i].price;
        //
        //                 if (res.data[i].price > high) {
        //                     high = res.data[i].price;
        //                 }
        //                 if (res.data[i].price < low) {
        //                     low = res.data[i].price;
        //                 }
        //                 date.setDate(date.getDate() + 1);
        //             }
        //         }
        //         const optionsMarketChart: any = {
        //             lineSmooth: Chartist.Interpolation.cardinal({
        //                 tension: 10
        //             }),
        //             axisY: {
        //                 showGrid: true,
        //                 offset: 40
        //             },
        //             axisX: {
        //                 showGrid: true,
        //             },
        //             high: Number(high) + 0.1,
        //             showPoint: true,
        //             height: '250px'
        //         };
        //
        //         const marketChart = new Chartist.Line('#marketchart', {
        //                 labels: labels,
        //                 series: [series]
        //             },
        //             optionsMarketChart);
        //
        //         this.startAnimationForLineChart(marketChart);
        //     }
        // });
    }

    onClickSellOrder(index) {
        this.buy.rate = Number(this.sellOrders[index].rate);
        this.buy.amount = Number(this.sellOrders[index].amount);
    }

    onClickBuyOrder(index) {
        this.sell.rate = Number(this.buyOrders[index].rate);
        this.sell.amount = Number(this.buyOrders[index].amount);
    }


    newBuyOrder() {
        if (this.buy.rate == '' || this.buy.amount == '') {
            return;
        }
        if (this.buy.rate == 0 || this.buy.amount == 0) {
            return;
        }

        this.buyloading = true;
        this.api.makeBTCBuyOrder(this.buy).subscribe(res => {

            this.buyloading = false;
            if (res.success) {
                this.notify.showNotification('Your order was posted successfully.', 'top', 'center', 'success');

                this.balance.getBtcBalance();
                this.balance.getTokenBalance();
                this.getOpenOrders();

                this.getSellOrders();
                this.getBuyOrders();
            } else {
                this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        }, err => {
            this.buyloading = false;
            this.notify.showNotification('Failed. Please try again.', 'top', 'center', 'warning');
        });
    }

    newSellOrder() {
        if (this.sell.rate == '' || this.sell.amount == '') {
            return;
        }
        if (this.sell.rate == 0 || this.sell.amount == 0) {
            return;
        }

        this.sellloading = true;
        this.api.makeBTCSellOrder(this.sell).subscribe(res => {
            this.sellloading = false;
            if (res.success) {
                this.notify.showNotification('Your order was posted successfully.', 'top', 'center', 'success');

                this.balance.getBtcBalance();
                this.balance.getTokenBalance();
                this.getOpenOrders();

                this.getSellOrders();
                this.getBuyOrders();
            } else {
                this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        }, err => {
            this.sellloading = false;
            this.notify.showNotification('Failed. Please try again.', 'top', 'center', 'warning');
        });
    }

    getBuyOrders() {
        this.api.getBTCBuyOrders({}).subscribe(res => {
            if (res.success) {
                this.buyOrders = res.data;
            }
        });
    }

    getSellOrders() {
        this.api.getBTCSellOrders({}).subscribe(res => {
            if (res.success) {
                this.sellOrders = res.data;
            }
        });
    }

    getPastOrders() {
        this.api.getBTCPastOrders({}).subscribe(res => {
            if (res.success) {
                this.pastOrders = res.data;
            }
        });
    }

    getOpenOrders() {
        this.api.getBTCOpenOrders({}).subscribe(res => {
            if (res.success) {
                this.openOrders = res.data;
            }
        });
    }

    cancelOrder(id) {
        let _parent = this;
        swal({
            title: 'Are you sure?',
            text: 'Your order will be deleted permanently.',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Yes',
            buttonsStyling: false
        }).then(function () {
            _parent.api.cancelBTCOrder({
                id: id
            }).subscribe(res => {
                if (res.success) {
                    _parent.notify.showNotification('Your order was canceled successfully.', 'top', 'center', 'success');

                    _parent.balance.getBtcBalance();
                    _parent.balance.getTokenBalance();
                    _parent.getOpenOrders();

                    _parent.getSellOrders();
                    _parent.getBuyOrders();
                } else {
                    _parent.notify.showNotification(res.error, 'top', 'center', 'warning');
                }
            });
        });
    }

}
