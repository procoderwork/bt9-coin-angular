import {Component, OnInit, AfterViewInit} from '@angular/core';
import {TableData} from '../../../md/md-table/md-table.component';
import {LegendItem, ChartType} from '../../../md/md-chart/md-chart.component';

import * as Chartist from 'chartist';
import {Api} from "../../../services/api.service";
import {SettingsService} from "../../../services/settings/settings.service";
import {BalanceService} from "../../../services/balance.service";
import {Notifications} from "../../../services/notifications.service";
import {StockChart} from "angular-highcharts";

declare const swal: any;
declare const $: any;

@Component({
    selector: 'app-exchangeeth',
    templateUrl: './exchangeeth.component.html',
    styleUrls: ['./exchangeeth.component.scss']
})
export class ExchangeETHComponent implements OnInit {
    icoChartData: any = [];

    buy: any = {};
    sell: any = {};

    buyOrders: any = [];
    sellOrders: any = [];

    openOrders: any = [];
    pastOrders: any = [];

    buyloading = false;
    sellloading = false;

    stockChart: any;
    exchangeInfo: any = {};

    eth_usd = 0;
    usd_eth = 0;
    constructor(
        public api: Api,
        public settings: SettingsService,
        public balance: BalanceService,
        public notify: Notifications
    ) {
    }

    startAnimationForLineChart(chart: any) {
        let seq: number, delays: number, durations: number;
        seq = 0;
        delays = 80;
        durations = 500;
        chart.on('draw', function (data: any) {

            if (data.type === 'line' || data.type === 'area') {
                data.element.animate({
                    d: {
                        begin: 600,
                        dur: 700,
                        from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
                        to: data.path.clone().stringify(),
                        easing: Chartist.Svg.Easing.easeOutQuint
                    }
                });
            } else if (data.type === 'point') {
                seq++;
                data.element.animate({
                    opacity: {
                        begin: seq * delays,
                        dur: durations,
                        from: 0,
                        to: 1,
                        easing: 'ease'
                    }
                });
            }
        });

        seq = 0;
    }

    ngOnInit() {
        this.getBuyOrders();
        this.getSellOrders();
        this.getPastOrders();
        this.getOpenOrders();

        this.api.getCoinRate({}).subscribe( res => {
            if (res.success) {
                this.eth_usd = res.eth_rate;
                this.usd_eth = 1 / Number(res.eth_rate);
            }
        }, err => {

        });

        this.api.getExchangeStatistics('ETH', {}).subscribe( res => {
            if (res.success) {
                this.exchangeInfo = res.data;
                if (this.exchangeInfo.lastPrice > 0) {
                    this.buy.rate = Number(this.exchangeInfo.lastPrice);
                    this.sell.rate = Number(this.exchangeInfo.lastPrice);
                }
            }
        });

        var _parent = this;
        this.api.getExchangeHistory('ETH', {}).subscribe(res => {
            if (res.success) {
                // split the data set into ohlc and volume
                var ohlc = [],
                    volume = [],
                    dataLength = res.data.length,
                    // set the allowed units for data grouping

                    groupingUnits = [[
                        'hour',                         // unit name
                        [6, 12, 24]                             // allowed multiples
                    ], [
                        'day',
                        [3, 7]
                    ]],

                    i = 0;

                for (i; i < res.data.length; i += 1) {
                    ohlc.push([
                        Number(res.data[i].date), // the date
                        Number(res.data[i].open), // open
                        Number(res.data[i].high), // high
                        Number(res.data[i].low), // low
                        Number(res.data[i].close) // close
                    ]);

                    volume.push([
                        Number(res.data[i].date), // the date
                        Number(res.data[i].volume) // the volume
                    ]);
                }
                // create the chart
                _parent.stockChart = new StockChart({

                    rangeSelector: {
                        selected: 0,
                        buttons: [{
                            type: 'hour',
                            count: 6,
                            text: '6h',
                        }, {
                            type: 'hour',
                            count: 12,
                            text: '12h'
                        }, {
                            type: 'hour',
                            count: 24,
                            text: '24h'
                        }, {
                            type: 'day',
                            count: 3,
                            text: '3d'
                        }, {
                            type: 'day',
                            count: 7,
                            text: '1w'
                        }, {
                            type: 'all',
                            text: 'All'
                        }]
                    },

                    title: {
                        text: 'Market History'
                    },

                    yAxis: [{
                        labels: {
                            align: 'right',
                            x: -3
                        },
                        title: {
                            text: 'Price'
                        },
                        height: '60%',
                        lineWidth: 2,
                        resize: {
                            enabled: true
                        }
                    }, {
                        labels: {
                            align: 'right',
                            x: -3
                        },
                        title: {
                            text: 'Volume'
                        },
                        top: '65%',
                        height: '35%',
                        offset: 0,
                        lineWidth: 2
                    }],

                    tooltip: {
                        split: true
                    },

                    series: [{
                        type: 'candlestick',
                        name: 'ETH Exchange Price',
                        data: ohlc,
                        dataGrouping: {
                            units: groupingUnits
                        }
                    }, {
                        type: 'column',
                        name: 'Volume(ETH)',
                        data: volume,
                        yAxis: 1,
                        dataGrouping: {
                            units: groupingUnits
                        }
                    }]
                });
            }
        });
    }

    onClickSellOrder(index) {
        this.buy.rate = Number(this.sellOrders[index].rate);
        this.buy.amount = Number(this.sellOrders[index].amount);
    }

    onClickBuyOrder(index) {
        this.sell.rate = Number(this.buyOrders[index].rate);
        this.sell.amount = Number(this.buyOrders[index].amount);
    }


    newBuyOrder() {
        if (this.buy.rate == '' || this.buy.amount == '') {
            return;
        }
        if (this.buy.rate == 0 || this.buy.amount == 0) {
            return;
        }

        this.buyloading = true;
        this.api.makeETHBuyOrder(this.buy).subscribe( res => {

            this.buyloading = false;
            if (res.success) {
                this.notify.showNotification('Your order was posted successfully.', 'top', 'center', 'success');

                this.balance.getEthBalance();
                this.balance.getTokenBalance();
                this.getOpenOrders();

                this.getSellOrders();
                this.getBuyOrders();
            } else {
                this.notify.showNotification( res.error, 'top', 'center',  'warning');
            }
        }, err => {
            this.buyloading = false;
            this.notify.showNotification('Failed. Please try again.', 'top', 'center', 'warning');
        });
    }

    newSellOrder() {
        if (this.sell.rate == '' || this.sell.amount == '') {
            return;
        }
        if (this.sell.rate == 0 || this.sell.amount == 0) {
            return;
        }

        this.sellloading = true;
        this.api.makeETHSellOrder(this.sell).subscribe( res => {
            this.sellloading = false;
            if (res.success) {
                this.notify.showNotification( 'Your order was posted successfully.', 'top', 'center', 'success');

                this.balance.getEthBalance();
                this.balance.getTokenBalance();
                this.getOpenOrders();

                this.getSellOrders();
                this.getBuyOrders();
            } else {
                this.notify.showNotification( res.error, 'top', 'center',  'warning');
            }
        }, err => {
            this.sellloading = false;
            this.notify.showNotification('Failed. Please try again.', 'top', 'center',  'warning');
        });
    }

    getBuyOrders() {
        this.api.getETHBuyOrders({}).subscribe( res => {
            if (res.success) {
                this.buyOrders = res.data;
            }
        });
    }

    getSellOrders() {
        this.api.getETHSellOrders({}).subscribe( res => {
            if (res.success) {
                this.sellOrders = res.data;
            }
        });
    }

    getPastOrders() {
        this.api.getETHPastOrders({}).subscribe( res => {
            if (res.success) {
                this.pastOrders = res.data;
            }
        });
    }

    getOpenOrders() {
        this.api.getETHOpenOrders({}).subscribe( res => {
            if (res.success) {
                this.openOrders = res.data;

            }
        });
    }

    cancelOrder(id) {
        let _parent = this;
        swal({
            title: 'Are you sure?',
            text: 'Your order will be deleted permanently.',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Yes',
            buttonsStyling: false
        }).then(function() {
            _parent.api.cancelETHOrder({
                id: id
            }).subscribe( res => {
                if (res.success) {
                    _parent.notify.showNotification('Your order was canceled successfully.', 'top', 'center', 'success');

                    _parent.balance.getEthBalance();
                    _parent.balance.getTokenBalance();
                    _parent.getOpenOrders();

                    _parent.getSellOrders();
                    _parent.getBuyOrders();
                } else {
                    _parent.notify.showNotification(res.error, 'top', 'center', 'warning');
                }
            });
        });
    }
}
