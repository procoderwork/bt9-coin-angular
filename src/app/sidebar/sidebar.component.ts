import {Component, OnInit} from '@angular/core';
import PerfectScrollbar from 'perfect-scrollbar';
import {SettingsService} from "../services/settings/settings.service";
import {Router} from "@angular/router";

declare const $: any;

//Metadata
export interface RouteInfo {
    path: string;
    title: string;
    type: string;
    icontype: string;
    collapse?: string;
    children?: ChildrenItems[];
}

export interface ChildrenItems {
    path: string;
    title: string;
    ab: string;
    type?: string;
}

//Menu Items
export const ROUTES: RouteInfo[] = [
    {
        path: '/dashboard',
        title: 'Dashboard',
        type: 'link',
        icontype: 'dashboard'
    }, {
        path: '/ico',
        title: 'ICO',
        type: 'link',
        icontype: 'donut_small'
    }, {
        path: '/wallet',
        title: 'Wallet',
        type: 'link',
        icontype: 'account_balance_wallet'
    }, {
        path: '/transaction',
        title: 'Transaction',
        type: 'link',
        icontype: 'assignment'
    }, {
        path: '/exchange',
        title: 'Exchange',
        type: 'sub',
        icontype: 'compare_arrows',
        collapse: 'exchange',
        children: [
            {path: 'btc', title: 'BTC Exchange', ab:''},
            {path: 'eth', title: 'ETH Exchange', ab:''},
        ]
    }, {
        path: '/lending',
        title: 'Lending',
        type: 'link',
        icontype: 'device_hub'
    }, {
        path: '/staking',
        title: 'Staking',
        type: 'link',
        icontype: 'play_for_work'
    }, {
        path: '/referral',
        title: 'Referral',
        type: 'link',
        icontype: 'share'
    }, {
        path: '/support',
        title: 'Support',
        type: 'link',
        icontype: 'forum'
    },
];

//Menu Items
export const ADMIN_ROUTES: RouteInfo[] = [
    {
        path: '/admin/dashboard',
        title: 'Dashboard',
        type: 'link',
        icontype: 'dashboard'
    },
    {
        path: '/admin/withdraw',
        title: 'Withdraw Pendings',
        type: 'link',
        icontype: 'build'
    },
    {
        path: '/admin/transaction',
        title: 'Transaction History',
        type: 'link',
        icontype: 'restore'
    },
    {
        path: '/admin/agenda',
        title: 'ICO Agenda',
        type: 'link',
        icontype: 'assignment'
    },
    {
        path: '/admin/lending',
        title: 'Lending',
        type: 'link',
        icontype: 'device_hub'
    }, {
        path: '/admin/staking',
        title: 'Staking',
        type: 'link',
        icontype: 'play_for_work'
    },
    {
        path: '/admin/users',
        title: 'Users',
        type: 'link',
        icontype: 'people'
    },
    {
        path: '/admin/settings',
        title: 'Settings',
        type: 'link',
        icontype: 'settings'
    },
    {
        path: '/admin/adminuser',
        title: 'Admin Users',
        type: 'link',
        icontype: 'person_add'
    }, {
        path: '/admin/support',
        title: 'Support',
        type: 'link',
        icontype: 'forum'
    },
];

@Component({
    selector: 'app-sidebar-cmp',
    templateUrl: 'sidebar.component.html',
})

export class SidebarComponent implements OnInit {
    public menuItems: any[];
    public fullname: any;

    constructor(
        public settings: SettingsService,
        private router: Router
    ) {
    }

    isMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };

    ngOnInit() {
        if (this.settings.getUserSetting('role') == 'admin') {
            this.menuItems = ADMIN_ROUTES.filter(menuItem => menuItem);
        } else {
            this.menuItems = ROUTES.filter(menuItem => menuItem);
        }
        this.fullname = this.settings.getUserSetting('fullname');
    }

    updatePS(): void {
        if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
            const elemSidebar = <HTMLElement>document.querySelector('.sidebar .sidebar-wrapper');
            let ps = new PerfectScrollbar(elemSidebar, {wheelSpeed: 2, suppressScrollX: true});
        }
    }

    isMac(): boolean {
        let bool = false;
        if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
            bool = true;
        }
        return bool;
    }

    onLogout() {
        this.settings.setAppSetting('is_loggedin', false);
        this.settings.user = {};
        this.settings.setStorage('token', false);

        this.router.navigate(['/login']);
    }

    onClickMenu() {
        $('.main-panel')[0].scrollTop = 0;
    }
}
