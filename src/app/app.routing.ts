import {Routes} from '@angular/router';

import {AdminLayoutComponent} from './layouts/admin/admin-layout.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {AuthGuard} from "./services/authguard.service";
import {SponserResolver} from "./register/sponser-resolver.service";
import {AuthLayoutComponent} from "./layouts/auth/auth-layout.component";
import {ResetPasswordComponent} from "./reset_password/reset_password.component";
import {ResetPasswordResolver} from "./reset_password/reset_password-resolver.service";

export const AppRoutes: Routes = [
    {
        path: '',
        component: AuthLayoutComponent,
        children: [
            {
                path: '',
                loadChildren: './pages/dashboard/dashboard.module#DashboardModule'
            },
            {
                path: 'dashboard',
                loadChildren: './pages/dashboard/dashboard.module#DashboardModule'
            },
            {
                path: 'ico',
                loadChildren: './pages/ico/ico.module#IcoModule'
            },
            {
                path: 'wallet',
                loadChildren: './pages/wallet/wallet.module#WalletModule'
            },
            {
                path: 'transaction',
                loadChildren: './pages/transaction/transaction.module#TransactionModule'
            },
            {
                path: 'exchange',
                loadChildren: './pages/exchange/exchange.module#ExchangeModule'
            },
            {
                path: 'lending',
                loadChildren: './pages/lending/lending.module#LendingModule'
            },
            {
                path: 'staking',
                loadChildren: './pages/staking/staking.module#StakingModule'
            },
            {
                path: 'referral',
                loadChildren: './pages/referral/referral.module#ReferralModule'
            },
            {
                path: 'security',
                loadChildren: './pages/security/security.module#SecurityModule'
            },
            {
                path: 'support',
                loadChildren: './pages/support/support.module#SupportModule'
            },
        ],
        resolve: {
            user: AuthGuard
        }
    },
    {
        path: 'admin',
        component: AdminLayoutComponent,
        children: [
            {
                path: '',
                loadChildren: './admin/admin.module#AdminModule'
            },
        ],
        resolve: {
            AuthGuard
        }
    },
    {path: 'login', component: LoginComponent},
    {path: 'login/confirmed', component: LoginComponent},
    {
        path: 'reset_password/:code',
        component: ResetPasswordComponent,
        resolve: {
            user_id: ResetPasswordResolver
        }
    },
    {path: 'register', component: RegisterComponent},
    {
        path: 'ref/:sponser',
        component: RegisterComponent,
        resolve: {
            sponser: SponserResolver
        }
    },
    {path: '**', redirectTo: 'dashboard'}
];
