import {Component, OnInit, AfterViewInit} from '@angular/core';
import {Api} from "../../services/api.service";
import {SettingsService} from "../../services/settings/settings.service";
import {Validate} from "../../services/validate.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {PasswordValidation} from "../../shared/components/password-validator.component";
import {Notifications} from "../../services/notifications.service";

declare const $: any;
declare var swal: any;

@Component({
    selector: 'app-admin-dashboard',
    templateUrl: './adminuser.component.html',
    styleUrls: ['./adminuser.component.scss']
})
export class AdminUserComponent implements OnInit, AfterViewInit {
    showAddModal = false;
    adminUsers: any = [];

    user: any = {
        permission: 0
    };
    signup: FormGroup;

    constructor(private formBuilder: FormBuilder,
                public api: Api,
                public settings: SettingsService,
                public validate: Validate,
                public notify: Notifications) {
    }

    ngOnInit() {
        this.loadAdminUsers();

        this.signup = this.formBuilder.group({
            // To add a validator, we must first convert the string value into an array. The first item in the array is the default value if any, then the next item in the array is the validator. Here we are adding a required validator meaning that the firstName attribute must have a value in it.
            username: [null, [Validators.required, Validators.pattern('^[a-zA-Z0-9]+$')]],
            fullname: [null, Validators.required],
            email: [null, [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$")]],
            // We can use more than one validator per field. If we want to use more than one validator we have to wrap our array of validators with a Validators.compose function. Here we are using a required, minimum length and maximum length validator.
            password: ['', Validators.required],
            confirmPassword: ['', Validators.required],
            permission: [''],
        }, {
            validator: PasswordValidation.MatchPassword // your validation method
        });
    }

    loadAdminUsers() {
        this.api.getAdminUsers({}).subscribe(res => {
            this.adminUsers = res.data;
        });
    }

    ngAfterViewInit() {

    }

    onEdit(user) {
        this.user = Object.assign({}, user);
        this.showAddModal = true;
    }

    onDelete(user) {
        let _parent = this;
        swal({
            title: 'Are you sure?',
            text: 'This user will be deleted permanently.',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Yes, delete it!',
            buttonsStyling: false
        }).then(function() {
            _parent.api.deleteAdmin({id: user.id}).subscribe(res => {
                if (res.success) {
                    _parent.notify.showNotification('Success', 'top', 'center', 'success');
                    _parent.loadAdminUsers();
                } else {
                    _parent.notify.showNotification(res.error, 'top', 'center', 'warning');
                }
            }, err => {
                _parent.notify.showNotification('Error', 'top', 'center', 'danger');
            });
        });
    }

    onAdd() {
        this.user = {
            id: 0,
            permission: 0
        };
        this.showAddModal = true;
    }

    addUser() {
        if ((this.user.id == 0 && this.signup.valid)
            || (this.user.id > 0)) {
            this.api.addAdmin(this.user).subscribe(res => {
                if (res.success) {
                    this.notify.showNotification('Success', 'top', 'center', 'success');
                    this.showAddModal = false;
                    this.loadAdminUsers();
                } else {
                    this.notify.showNotification(res.error, 'top', 'center', 'warning');
                }
            }, err => {
                this.notify.showNotification('Error', 'top', 'center', 'danger');
            });
        } else {
            this.validate.validateAllFormFields(this.signup);
        }
    }
}
