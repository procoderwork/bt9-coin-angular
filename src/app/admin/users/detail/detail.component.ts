import {Component, OnInit, AfterViewInit} from '@angular/core';
import {TableData} from '../../../md/md-table/md-table.component';
import {LegendItem, ChartType} from '../../../md/md-chart/md-chart.component';

import * as Chartist from 'chartist';
import {Api} from "../../../services/api.service";
import {SettingsService} from "../../../services/settings/settings.service";
import {BalanceService} from "../../../services/balance.service";
import {ActivatedRoute, Router} from "@angular/router";

declare const $: any;
declare var swal: any;

@Component({
    selector: 'app-coming',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss']
})
export class UserDetailComponent implements OnInit, AfterViewInit {
    // constructor(private navbarTitleService: NavbarTitleService, private notificationService: NotificationService) { }
    user: any = {};

    tokenHistory: any = [];
    btcHistory: any = [];
    ethHistory: any = [];

    touch: boolean;
    tx: any = {
        date: new Date()
    };
    showModal = false;

    tokendatatable: any = null;
    btcdatatable: any = null;
    ethdatatable: any = null;

    constructor(private api: Api,
                public settings: SettingsService,
                public balance: BalanceService,
                public route: ActivatedRoute,
                private router: Router) {

    }

    public ngOnInit() {
        let user_id = this.route.snapshot.params['userid'];

        this.api.getUser({
            id: user_id
        }).subscribe(res => {
            if (res.success) {
                this.user = res.data;

                this.loadTokenHistory();
                // this.loadBTCReceivedTxList();
                // this.loadETHReceivedTxList();

                // this.loadBTCSentTxList();
                /// this.loadETHSentTxList();
            } else {
                this.router.navigate(['/admin/dashboard']);
            }
        }, err => {
            this.router.navigate(['/admin/dashboard']);
        });
    }

    buildTokenDataTable() {
        this.tokendatatable = $('#myaltcoindatatable').DataTable({
            'pagingType': 'full_numbers',
            'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
            responsive: true,
            language: {
                search: '_INPUT_',
                searchPlaceholder: 'Search records',
            }
        });

    }

    loadTokenHistory() {
        if (this.tokendatatable) {
            this.tokendatatable.destroy();
        }
        this.api.getAdminUserTokenHistory({
            user_id: this.user.id
        }).subscribe(res => {
            if (res.success) {
                this.tokenHistory = res.data;

                let _parent = this;
                var timerID = setInterval(function () {
                    if ($('#myaltcoindatatable tbody tr').length == res.data.length) {
                        clearInterval(timerID);
                        _parent.tokendatatable = $('#myaltcoindatatable').DataTable({});
                    }
                }, 200);
            }
        });
    }


    loadBTCHistory() {
        if (this.btcdatatable) {
            this.btcdatatable.destroy();
        }
        this.api.getAdminUserBTCHistory({
            user_id: this.user.id
        }).subscribe(res => {
            if (res.success) {
                this.btcHistory = res.data;

                let _parent = this;
                var timerID = setInterval(function () {
                    if ($('#btcdatatable tbody tr').length == res.data.length) {
                        clearInterval(timerID);
                        _parent.btcdatatable = $('#btcdatatable').DataTable({});
                    }
                }, 200);
            }
        });
    }

    loadETHHistory() {
        if (this.ethdatatable) {
            this.ethdatatable.destroy();
        }
        this.api.getAdminUserETHHistory({
            user_id: this.user.id
        }).subscribe(res => {
            if (res.success) {
                this.ethHistory = res.data;

                let _parent = this;
                var timerID = setInterval(function () {
                    if ($('#ethdatatable tbody tr').length == res.data.length) {
                        clearInterval(timerID);
                        _parent.ethdatatable = $('#ethdatatable').DataTable({});
                    }
                }, 200);
            }
        });
    }

    ngAfterViewInit() {

    }

    addMyAltCoin() {
        this.showModal = true;
    }

    submit() {
        if (this.tx.coin == null || this.tx.coin == '') {
            return;
        }
        if (this.tx.coin_amount == null || this.tx.coin_amount == '') {
            return;
        }
        if (this.tx.myaltcoin_amount == null || this.tx.myaltcoin_amount == '') {
            return;
        }

        var date = this.tx.date.getFullYear() + '-' + (this.tx.date.getMonth() + 1) + '-' + (this.tx.date.getDate());

        this.api.addMyAltCoin({
            user_id: this.user.id,
            date: date,
            src_currency: this.tx.coin,
            src_amount: this.tx.coin_amount,
            dest_amount: this.tx.myaltcoin_amount
        }).subscribe( res => {
            if (res.success) {
                this.showModal = false;
                this.loadTokenHistory();
            }
        }, err => {

        });
    }
}
