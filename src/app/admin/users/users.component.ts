import {Component, OnInit, AfterViewInit} from '@angular/core';
import {TableData} from '../../md/md-table/md-table.component';
import {LegendItem, ChartType} from '../../md/md-chart/md-chart.component';

import * as Chartist from 'chartist';
import {Api} from "../../services/api.service";
import {SettingsService} from "../../services/settings/settings.service";
import {BalanceService} from "../../services/balance.service";
import {Router} from "@angular/router";

declare const $: any;
declare var swal: any;

@Component({
    selector: 'app-users',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit, AfterViewInit {
    // constructor(private navbarTitleService: NavbarTitleService, private notificationService: NotificationService) { }
    users: any = [];

    constructor(
        private router: Router,
        private api: Api,
        public settings: SettingsService,
        public balance: BalanceService
    ) { }

    public ngOnInit() {
        this.api.getUsers().subscribe( res => {
            this.users = res.data;

            let _parent = this;
            var timerID = setInterval(function() {
                if ($('#userstable tbody tr').length == res.data.length) {
                    clearInterval(timerID);
                    _parent.buildUsersDataTable();
                }
            }, 200);
        });
    }


    buildUsersDataTable() {
        $('#userstable').DataTable({
            'pagingType': 'full_numbers',
            'lengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
            responsive: true,
            language: {
                search: '_INPUT_',
                searchPlaceholder: 'Search records',
            }
        });

        const table = $('#userstable').DataTable();

        // Edit record
        table.on( 'click', '.view', function () {
            const $tr = $(this).closest('tr');

            const data = table.row($tr).data();

            _parent.api.getUserIDByEmail({
                email: data[0]
            }).subscribe( res => {
                if (res.success) {
                    _parent.router.navigate(['/admin/users/detail/' + res.id]);
                }
            });
        } );

        let _parent = this;
        // Delete a record
        table.on( 'click', '.remove', function (e: any) {
            const $tr = $(this).closest('tr');
            const data = table.row($tr).data();

            swal({
                title: 'Are you sure?',
                text: 'This user will be deleted permanently.',
                type: 'warning',
                showCancelButton: true,
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                confirmButtonText: 'Yes, delete it!',
                buttonsStyling: false
            }).then(function() {
                _parent.api.deleteUser({
                    email: data[0]
                }).subscribe( res => {
                    if (res.success) {
                        table.row($tr).remove().draw();
                        e.preventDefault();
                        swal({
                            title: 'Deleted!',
                            text: 'Your file has been deleted.',
                            type: 'success',
                            confirmButtonClass: 'btn btn-success',
                            buttonsStyling: false
                        });
                    } else {
                        swal({
                            title: 'Failed!',
                            text: 'This user doest not exist.',
                            type: 'warning',
                            confirmButtonClass: 'btn btn-success',
                            buttonsStyling: false
                        });
                    }
                });
            });
        } );

    }

    ngAfterViewInit() {

    }

}
