import {Routes, RouterModule} from '@angular/router';
import {AdminTicketComponent} from "./ticket.component";
import {AdminTicketDetailComponent} from "./detail/detail.component";

export const AdminTicketRoutes: Routes = [
    {
        path: '',
        component: AdminTicketComponent
    },
    {
        path: 'detail/:id',
        component: AdminTicketDetailComponent
    }
];
