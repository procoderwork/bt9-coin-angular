import {Component, OnInit, AfterViewInit} from '@angular/core';
import {Api} from "../../services/api.service";
import {SettingsService} from "../../services/settings/settings.service";
import {BalanceService} from "../../services/balance.service";

import * as Chartist from 'chartist';
declare const $: any;
declare var swal: any;

@Component({
    selector: 'app-admin-transaction',
    templateUrl: './transaction.component.html',
    styleUrls: ['./transaction.component.scss']
})
export class AdminTransactionComponent implements OnInit, AfterViewInit {
    bt9TxList: any = [];
    btcTxList: any = [];
    ethTxList: any = [];

    constructor(
        private api: Api,
        public settings: SettingsService,
        public balance: BalanceService
    ) { }

    ngOnInit() {
        this.api.getAllTransactionHistory('BT9', {}).subscribe( res => {
            if (res.success) {
                this.bt9TxList = res.data;
            }
        });

        this.api.getAllTransactionHistory('BTC', {}).subscribe( res => {
            if (res.success) {
                this.btcTxList = res.data;
            }
        });

        this.api.getAllTransactionHistory('ETH', {}).subscribe( res => {
            if (res.success) {
                this.ethTxList = res.data;
            }
        });
    }

    ngAfterViewInit() {

    }
}
