import {Component, OnInit, AfterViewInit} from '@angular/core';
import {Api} from "../../services/api.service";
import {SettingsService} from "../../services/settings/settings.service";
import {Notifications} from "../../services/notifications.service";

declare const $: any;
declare var swal: any;

@Component({
    selector: 'app-admin-settings',
    templateUrl: './staking.component.html',
    styleUrls: ['./staking.component.scss']
})
export class AdminStakingComponent implements OnInit, AfterViewInit {
    stakings: any = [];

    constructor(public api: Api,
                public settings: SettingsService,
                public notify: Notifications) {
    }

    ngOnInit() {
        this.loadStakings();
    }

    loadStakings() {
        this.api.getAllStaking({}).subscribe( res => {
            if (res.success) {
                this.stakings = res.data;
            }
        });
    }
    ngAfterViewInit() {

    }

    releaseAllStaking() {
        let _parent = this;
        swal({
            title: 'Are you sure?',
            text: 'Release All Staking-Capital?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Yes',
            buttonsStyling: false
        }).then(function() {
            _parent.settings.loading = true;
            _parent.api.releaseAllStaking({}).subscribe( res => {
                _parent.settings.loading = false;
                if (res.success) {
                    _parent.notify.showNotification('Success', 'top', 'center', 'success');
                } else {
                    _parent.notify.showNotification(res.error, 'top', 'center', 'warning');
                }
            }, err => {
                _parent.settings.loading = false;
            });
        });
    }
}
