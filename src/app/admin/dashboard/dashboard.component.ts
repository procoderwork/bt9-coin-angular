import {Component, OnInit, AfterViewInit} from '@angular/core';
import {Api} from "../../services/api.service";
import {SettingsService} from "../../services/settings/settings.service";
import {BalanceService} from "../../services/balance.service";

import * as Chartist from 'chartist';
declare const $: any;
declare var swal: any;

@Component({
    selector: 'app-admin-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class AdminDashboardComponent implements OnInit, AfterViewInit {
    usersts: any = {};
    icosts: any = {};
    lendingsts: any = {};
    stakingsts: any = {};
    wallet: any = {};
    fee: any = {};

    constructor(
        private api: Api,
        public settings: SettingsService,
        public balance: BalanceService
    ) { }

    ngOnInit() {
        this.api.getUserStatistics({}).subscribe(res => {
            if (res.success) {
                this.usersts = res.data;
            }
        });
        this.api.getICOStatistics({}).subscribe(res => {
            if (res.success) {
                this.icosts = res.data;
            }
        });
        this.api.getLendingStatistics({}).subscribe(res => {
            if (res.success) {
                this.lendingsts = res.data;
            }
        });
        this.api.getStakingStatistics({}).subscribe(res => {
            if (res.success) {
                this.stakingsts = res.data;
            }
        });
        this.api.getWalletTotalBalance('BTC').subscribe( res => {
           if (res.success) {
               this.wallet.btc_balance = res.balance;
           }
        });
        this.api.getWalletTotalBalance('ETH').subscribe( res => {
            if (res.success) {
                this.wallet.eth_balance = res.balance;
            }
        });
        this.api.getWalletTotalBalance('BT9').subscribe( res => {
            if (res.success) {
                this.wallet.bt9_balance = res.balance;
            }
        });
        this.api.getFeeStatistics({}).subscribe(res => {
            if (res.success) {
                this.fee = res.data;
            }
        });
    }

    ngAfterViewInit() {

    }
}
