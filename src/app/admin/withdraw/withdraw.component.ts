import {Component, OnInit, AfterViewInit} from '@angular/core';
import {Api} from "../../services/api.service";
import {SettingsService} from "../../services/settings/settings.service";
import {BalanceService} from "../../services/balance.service";

import * as Chartist from 'chartist';
import {Notifications} from "../../services/notifications.service";
declare const $: any;
declare var swal: any;

@Component({
    selector: 'app-admin-withdraw',
    templateUrl: './withdraw.component.html',
    styleUrls: ['./withdraw.component.scss']
})
export class AdminWithdrawComponent implements OnInit, AfterViewInit {
    bt9Pending: any = [];
    btcPending: any = [];
    ethPending: any = [];

    bt9info: any = {
        total_balance: 0,
        estimated_fee: 0,
        profit_fee: 0,
        priority: 0,
        hex: ''
    };

    btcinfo: any = {
        total_balance: 0,
        estimated_fee: 0,
        profit_fee: 0,
        priority: 0,
        hex: ''
    };

    constructor(
        private api: Api,
        public settings: SettingsService,
        public balance: BalanceService,
        public notify: Notifications
    ) { }

    ngOnInit() {
        this.loadBT9Pending();
    }

    loadBT9Pending() {
        this.api.getWithdrawPending('BT9').subscribe( res => {
            if (res.success) {
                this.bt9Pending = res.data;

                this.bt9info.total_number = res.data.length;
                this.bt9info.total_amount = 0;
                this.bt9info.total_fee = 0;

                for (let i = 0; i < this.bt9Pending.length; i ++) {
                    this.bt9info.total_amount += Number(this.bt9Pending[i].src_amount - this.bt9Pending[i].fee);
                    this.bt9info.total_fee += Number(this.bt9Pending[i].fee);
                }
            }
        });

        this.api.getWalletTotalBalance('BT9').subscribe(res => {
            if (res.success) {
                this.bt9info.total_balance = res.balance;
            }
        });
    }

    estimateBT9Fee() {
        this.settings.loading = true;
        this.api.estimateFee('BT9', {
            priority: this.bt9info.priority
        }).subscribe(res => {
            this.settings.loading = false;
            if (res.success) {
                // this.bt9info.estimated_fee = res.fee;
                // this.bt9info.profit_fee = this.bt9info.total_fee - this.bt9info.estimated_fee;
                this.bt9info.hex = res.hex;

                this.notify.showNotification('You can withdraw now, Please click Withdraw Button', 'top', 'center', 'success');
            } else {
                this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        }, err => {
            this.settings.loading = false;
            this.notify.showNotification('Please try again.', 'top', 'center', 'danger');
        });
    }

    withdrawBT9() {
        // if (this.bt9info.hex == '') {
        //     this.notify.showNotification('You have to check.', 'top', 'center', 'warning');
        //     return;
        // }

        this.settings.loading = true;
        this.api.withdrawPending('BT9', {
            // hex: this.bt9info.hex,
            // fee: this.bt9info.estimated_fee
            fee: 0
        }).subscribe(res => {
            this.settings.loading = false;
            if (res.success) {
                this.notify.showNotification('Withdraw Successfully', 'top', 'center', 'success');
                this.loadBT9Pending();
            } else {
                this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        }, err => {
            this.settings.loading = false;
            this.notify.showNotification('Please try again', 'top', 'center', 'warning');
        });
    }

    loadBTCPending() {
        this.api.getWithdrawPending('BTC').subscribe( res => {
            if (res.success) {
                this.btcPending = res.data;

                this.btcinfo.total_number = res.data.length;
                this.btcinfo.total_amount = 0;
                this.btcinfo.total_fee = 0;

                for (let i = 0; i < this.btcPending.length; i ++) {
                    this.btcinfo.total_amount += Number(this.btcPending[i].src_amount - this.btcPending[i].fee);
                    this.btcinfo.total_fee += Number(this.btcPending[i].fee);
                }
            }
        });

        this.api.getWalletTotalBalance('BTC').subscribe(res => {
            if (res.success) {
                this.btcinfo.total_balance = res.balance;
            }
        });
    }

    estimateBTCFee() {
        this.settings.loading = true;
        this.api.estimateFee('BTC', {
            priority: this.btcinfo.priority
        }).subscribe(res => {
            this.settings.loading = false;
            if (res.success) {
                this.btcinfo.estimated_fee = res.fee;
                this.btcinfo.profit_fee = this.btcinfo.total_fee - this.btcinfo.estimated_fee;
                this.btcinfo.hex = res.hex;
            } else {
                this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        }, err => {
            this.settings.loading = false;
            this.notify.showNotification('Please try again.', 'top', 'center', 'danger');
        });
    }

    withdrawBTC() {
        if (this.btcinfo.hex == '') {
            this.notify.showNotification('You have to estimate fee', 'top', 'center', 'warning');
            return;
        }

        this.settings.loading = true;
        this.api.withdrawPending('BTC', {
            hex: this.btcinfo.hex,
            fee: this.btcinfo.estimated_fee
        }).subscribe(res => {
            this.settings.loading = false;
            if (res.success) {
                this.notify.showNotification('Withdraw Successfully', 'top', 'center', 'success');
                this.loadBT9Pending();
            } else {
                this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        }, err => {
            this.settings.loading = false;
            this.notify.showNotification('Please try again', 'top', 'center', 'warning');
        });
    }

    loadETHPending() {
        this.api.getWithdrawPending('ETH').subscribe( res => {
            if (res.success) {
                this.ethPending = res.data;
            }
        });
    }

    ngAfterViewInit() {

    }
}
