import {Component, OnInit, AfterViewInit} from '@angular/core';
import {Api} from "../../services/api.service";
import {SettingsService} from "../../services/settings/settings.service";
import {Notifications} from "../../services/notifications.service";

declare const $: any;
declare var swal: any;

@Component({
    selector: 'app-admin-settings',
    templateUrl: './lending.component.html',
    styleUrls: ['./lending.component.scss']
})
export class AdminLendingComponent implements OnInit, AfterViewInit {
    lendings: any = [];

    lendingSettings: any = [];
    lendingSetting: any = {};

    touch: boolean;
    showModal = false;

    lendingStartDate: any;

    sysSettings: any = {};
    constructor(public api: Api,
                public settings: SettingsService,
                public notify: Notifications) {
    }

    ngOnInit() {
        this.loadLendings();
        this.loadLendingSettings();

        this.api.getSettings().subscribe(res => {
            this.lendingStartDate = res.data.lending_start_date ? res.data.lending_start_date : '';
        });
    }

    loadLendings() {
        this.api.getAllLending({}).subscribe( res => {
            if (res.success) {
                this.lendings = res.data;
            }
        });
    }

    loadLendingSettings() {
        this.api.getLendingSetting({}).subscribe( res => {
           if (res.success) {
               this.lendingSettings = res.data;
           }
        });
    }

    onAdd() {
        this.lendingSetting = {
            id: 0
        };
        this.showModal = true;
    }

    onEdit(setting) {
        this.lendingSetting = setting;
        this.showModal = true;
    }

    onDelete(id) {
        this.api.deleteLendingSetting({
            id: id
        }).subscribe( res => {
            if (res.success) {
                this.notify.showNotification('Deleted Successfully', 'top', 'center', 'success');
                this.loadLendingSettings();
            } else {
                this.notify.showNotification('Failed', 'top', 'center', 'warning');
            }
        }, err => {
            this.notify.showNotification('Failed', 'top', 'center', 'warning');
        });
    }
    saveLendingSetting() {
        this.api.saveLendingSetting(this.lendingSetting).subscribe( res => {
            if (res.success) {
                this.notify.showNotification('Saved Succesfully', 'top', 'center', 'success');
                this.showModal = false;
                this.loadLendingSettings();
            } else {
                this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        }, err => {
            this.notify.showNotification('Failed', 'top', 'center', 'warning');
        });
    }

    saveStartDate() {
        if (this.lendingStartDate == '') {
            return;
        }

        var year = this.lendingStartDate.getFullYear();
        var month = this.lendingStartDate.getMonth() + 1;
        var date = this.lendingStartDate.getDate();

        if (month < 10) month = '0' + month;
        if (date < 10) date = '0' + date;

        var start_date = year + '-' + month + '-' + date;

        this.api.saveSettings({
            lending_start_date: start_date
        }).subscribe( res => {
            if (res.success) {
                this.notify.showNotification('Success', 'top', 'center', 'success');
            } else {
                this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        });
    }
    ngAfterViewInit() {

    }

    releaseAllLending() {

        let _parent = this;
        swal({
            title: 'Are you sure?',
            text: 'Release All Lending-Capital?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            confirmButtonText: 'Yes',
            buttonsStyling: false
        }).then(function() {
            _parent.settings.loading = true;
            _parent.api.releaseAllLending({}).subscribe( res => {
                _parent.settings.loading = false;
                if (res.success) {
                    _parent.notify.showNotification('Success', 'top', 'center', 'success');
                } else {
                    _parent.notify.showNotification(res.error, 'top', 'center', 'warning');
                }
            }, err => {
                _parent.settings.loading = false;
            });
        });
    }
}
