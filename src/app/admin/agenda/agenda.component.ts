import {Component, OnInit, AfterViewInit} from '@angular/core';
import {Api} from "../../services/api.service";
import {SettingsService} from "../../services/settings/settings.service";
import {Notifications} from "../../services/notifications.service";

declare const $: any;
declare var swal: any;

@Component({
    selector: 'app-admin-settings',
    templateUrl: './agenda.component.html',
    styleUrls: ['./agenda.component.scss']
})
export class AgendaComponent implements OnInit, AfterViewInit {
    agendas: any = [];
    agenda: any = {};

    histories: any = [];

    touch: boolean;
    showModal = false;

    datatable: any = null;
    constructor(public api: Api,
                public settings: SettingsService,
                public notify: Notifications) {
    }

    ngOnInit() {
        this.loadICOAgenda();
        this.loadICOHistory();
    }

    loadICOAgenda() {
        this.api.getICOAgenda({}).subscribe( res => {
            if (res.success) {
                this.agendas = res.data;
            }
        });
    }

    loadICOHistory() {
        if (this.datatable) {
            this.datatable.destroy();
        }

        this.api.getAllICOHistory({}).subscribe( res => {
            if (res.success) {
                this.histories = res.data;

                let _parent = this;
                var timerID = setInterval(function() {
                    if ($('#historytable tbody tr').length == res.data.length) {
                        clearInterval(timerID);
                        _parent.datatable = $('#historytable').DataTable({});
                    }
                }, 200);
            }
        });
    }

    onAdd() {
        this.agenda = {
            id: 0
        };
        this.showModal = true;
    }

    onEdit(agenda) {
        this.agenda = agenda;
        this.showModal = true;
    }

    onDelete(id) {
        this.api.deleteICOAgenda({
            id: id
        }).subscribe( res => {
            if (res.success) {
                this.notify.showNotification('Deleted Successfully', 'top', 'center', 'success');
                this.loadICOAgenda();
            } else {
                this.notify.showNotification('Failed', 'top', 'center', 'warning');
            }
        }, err => {
            this.notify.showNotification('Failed', 'top', 'center', 'warning');
        });
    }
    saveICOAgenda() {
        if (this.agenda.from.getFullYear) {
            var date = this.agenda.from.getFullYear() + '-' + (this.agenda.from.getMonth() + 1) + '-' + (this.agenda.from.getDate());
            this.agenda.from = date;
        }

        if (this.agenda.to.getFullYear) {
            var date = this.agenda.to.getFullYear() + '-' + (this.agenda.to.getMonth() + 1) + '-' + (this.agenda.to.getDate());
            this.agenda.to = date;
        }
        this.api.saveICOAgenda(this.agenda).subscribe( res => {
            if (res.success) {
                this.notify.showNotification('Saved Succesfully', 'top', 'center', 'success');
                this.showModal = false;
                this.loadICOAgenda();
            } else {
                this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        }, err => {
            this.notify.showNotification('Failed', 'top', 'center', 'warning');
        });
    }

    ngAfterViewInit() {

    }
}
