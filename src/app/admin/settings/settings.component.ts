import {Component, OnInit, AfterViewInit} from '@angular/core';
import {Api} from "../../services/api.service";
import {SettingsService} from "../../services/settings/settings.service";
import {Notifications} from "../../services/notifications.service";

declare const $: any;
declare var swal: any;

@Component({
    selector: 'app-admin-settings',
    templateUrl: './settings.component.html',
    styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit, AfterViewInit {
    sysSettings: any = {};
    bonusLists: any = [];
    tokenRates: any = [];

    constructor(public api: Api,
                public settings: SettingsService,
                public notify: Notifications) {
    }

    ngOnInit() {
        this.api.getSettings().subscribe(res => {
            this.sysSettings = res.data;

            this.sysSettings.login_recaptcha = this.sysSettings.login_recaptcha == 1 ? true : false;
            this.sysSettings.register_recaptcha = this.sysSettings.register_recaptcha == 1 ? true : false;
            this.sysSettings.ico_recaptcha = this.sysSettings.ico_recaptcha  == 1 ? true : false;


            this.sysSettings.enable_btc_wallet = this.sysSettings.enable_btc_wallet  == 1 ? true : false;
            this.sysSettings.enable_eth_wallet = this.sysSettings.enable_eth_wallet  == 1 ? true : false;
            this.sysSettings.enable_btc_withdraw = this.sysSettings.enable_btc_withdraw  == 1 ? true : false;
            this.sysSettings.enable_eth_withdraw = this.sysSettings.enable_eth_withdraw  == 1 ? true : false;
            //this.sysSettings.btc_wallet_mode = this.sysSettings.btc_wallet_mode  == 1 ? true : false;
            //this.sysSettings.eth_wallet_mode = this.sysSettings.eth_wallet_mode  == 1 ? true : false;

            this.sysSettings.enable_reg_notification = this.sysSettings.enable_reg_notification == 1 ? true : false;

            this.sysSettings.enable_btc_exchange = this.sysSettings.enable_btc_exchange == 1 ? true : false;
            this.sysSettings.enable_eth_exchange = this.sysSettings.enable_eth_exchange == 1 ? true : false;

            this.sysSettings.enable_bt9_wallet = this.sysSettings.enable_bt9_wallet == 1 ? true : false;
            this.sysSettings.enable_bt9_withdraw = this.sysSettings.enable_bt9_wallet == 1 ? true : false;

            this.sysSettings.enable_lending = this.sysSettings.enable_lending == 1 ? true : false;
            this.sysSettings.enable_staking = this.sysSettings.enable_staking == 1 ? true : false;
            this.sysSettings.enable_ico = this.sysSettings.enable_ico == 1 ? true : false;
            this.sysSettings.enable_support = this.sysSettings.enable_support == 1 ? true : false;
        });

        this.api.getBonusList({}).subscribe( res => {
            this.bonusLists = res.data;

        });

        this.api.getTokenRate({}).subscribe( res => {
            this.tokenRates = res.data;
        });
    }

    ngAfterViewInit() {

    }

    saveBonus() {
        this.api.saveBonus(this.bonusLists).subscribe( res => {
            if (res.success) {
                this.notify.showNotification('Success', 'top', 'center', 'success');
            } else {
                this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        });
    }

    saveRates() {
        this.api.saveRates(this.tokenRates).subscribe( res => {
            if (res.success) {
                this.notify.showNotification('Success', 'top', 'center', 'success');
            } else {
                this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        });
    }

    saveRecaptchaSetting() {
        this.api.saveSettings({
            login_recaptcha: this.sysSettings.login_recaptcha ? 1 : 0,
            register_recaptcha: this.sysSettings.register_recaptcha ? 1 : 0,
            ico_recaptcha: this.sysSettings.ico_recaptcha ? 1 : 0
        }).subscribe( res => {
            if (res.success) {
                this.notify.showNotification('Success', 'top', 'center', 'success');
            } else {
                this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        });
    }

    saveSystemSetting() {
        this.api.saveSettings({
            blockcypher_token: this.sysSettings.blockcypher_token,
            disconnect_time: this.sysSettings.disconnect_time,
            enable_lending: this.sysSettings.enable_lending ? 1 : 0,
            enable_staking: this.sysSettings.enable_staking ? 1 : 0,
            enable_ico: this.sysSettings.enable_ico ? 1 : 0,
            enable_support: this.sysSettings.enable_support ? 1 : 0,
            enable_reg_notification: this.sysSettings.enable_reg_notification ? 1 : 0
        }).subscribe( res => {
            if (res.success) {
                this.notify.showNotification('Success', 'top', 'center', 'success');
            } else {
                this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        });
    }

    saveFeeSetting() {
        this.api.saveSettings({
            withdraw_btc_fee: this.sysSettings.withdraw_btc_fee,
            withdraw_eth_fee: this.sysSettings.withdraw_eth_fee,
            withdraw_bt9_fee: this.sysSettings.withdraw_bt9_fee,
            exchange_buy_fee: this.sysSettings.exchange_buy_fee,
            exchange_sell_fee: this.sysSettings.exchange_sell_fee
        }).subscribe( res => {
            if (res.success) {
                this.notify.showNotification('Success', 'top', 'center', 'success');
            } else {
                this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        });
    }

    saveWalletSetting() {
        this.api.saveSettings({
            enable_btc_wallet: this.sysSettings.enable_btc_wallet ? 1 : 0,
            enable_eth_wallet: this.sysSettings.enable_eth_wallet ? 1 : 0,
            enable_bt9_wallet: this.sysSettings.enable_bt9_wallet ? 1 : 0,
            enable_btc_withdraw: this.sysSettings.enable_btc_withdraw ? 1 : 0,
            enable_eth_withdraw: this.sysSettings.enable_eth_withdraw ? 1 : 0,
            enable_bt9_withdraw: this.sysSettings.enable_bt9_withdraw ? 1 : 0,
            // btc_wallet_mode: this.sysSettings.btc_wallet_mode ? 1 : 0,
            // eth_wallet_mode: this.sysSettings.eth_wallet_mode ? 1 : 0,
            withdraw_btc_volume_day: this.sysSettings.withdraw_btc_volume_day,
            withdraw_btc_volume_month: this.sysSettings.withdraw_btc_volume_month,
            withdraw_eth_volume_day: this.sysSettings.withdraw_eth_volume_day,
            withdraw_eth_volume_month: this.sysSettings.withdraw_eth_volume_month,
            withdraw_myaltcoin_volume_day: this.sysSettings.withdraw_myaltcoin_volume_day,
            withdraw_myaltcoin_volume_month: this.sysSettings.withdraw_myaltcoin_volume_month
        }).subscribe( res => {
            if (res.success) {
                this.notify.showNotification('Success', 'top', 'center', 'success');
            } else {
                this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        });
    }

    saveExchangeSetting() {
        this.api.saveSettings({
            enable_btc_exchange: this.sysSettings.enable_btc_exchange ? 1 : 0,
            enable_eth_exchange: this.sysSettings.enable_eth_exchange ? 1 : 0
        }).subscribe( res => {
            if (res.success) {
                this.notify.showNotification('Success', 'top', 'center', 'success');
            } else {
                this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        });

    }

    saveStakingSetting() {
        this.api.saveSettings({
            staking_locked_days: this.sysSettings.staking_locked_days,
            staking_interest_month: this.sysSettings.staking_interest_month,
            staking_minimum_amount: this.sysSettings.staking_minimum_amount
        }).subscribe( res => {
            if (res.success) {
                this.notify.showNotification('Success', 'top', 'center', 'success');
            } else {
                this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        });
    }

    saveLendingSetting() {
        this.api.saveSettings({
            lending_interest_min: this.sysSettings.lending_interest_min,
            lending_interest_max: this.sysSettings.lending_interest_max
        }).subscribe( res => {
            if (res.success) {
                this.notify.showNotification('Success', 'top', 'center', 'success');
            } else {
                this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        });
    }

    saveBT9RPCSetting() {
        this.api.saveSettings({
            bt9_rpcserver: this.sysSettings.bt9_rpcserver,
            bt9_rpcuser: this.sysSettings.bt9_rpcuser,
            bt9_rpcpassword: this.sysSettings.bt9_rpcpassword,
            bt9_rpcport: this.sysSettings.bt9_rpcport
        }).subscribe( res => {
            if (res.success) {
                this.notify.showNotification('Success', 'top', 'center', 'success');
            } else {
                this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        });
    }

    saveBTCRPCSetting() {
        this.api.saveSettings({
            btc_rpcserver: this.sysSettings.btc_rpcserver,
            btc_rpcuser: this.sysSettings.btc_rpcuser,
            btc_rpcpassword: this.sysSettings.btc_rpcpassword,
            btc_rpcport: this.sysSettings.btc_rpcport
        }).subscribe( res => {
            if (res.success) {
                this.notify.showNotification('Success', 'top', 'center', 'success');
            } else {
                this.notify.showNotification(res.error, 'top', 'center', 'warning');
            }
        });
    }
}
