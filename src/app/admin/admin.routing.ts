import {Routes, RouterModule} from '@angular/router';
import {AdminDashboardComponent} from "./dashboard/dashboard.component";
import {AdminUserComponent} from "./adminuser/adminuser.component";
import {UsersComponent} from "./users/users.component";
import {UserDetailComponent} from "./users/detail/detail.component";
import {SettingsComponent} from "./settings/settings.component";
import {AgendaComponent} from "./agenda/agenda.component";
import {AdminLendingComponent} from "./lending/lending.component";
import {AdminWithdrawComponent} from "./withdraw/withdraw.component";
import {AdminTransactionComponent} from "./transaction/transaction.component";
import {AdminStakingComponent} from "./staking/staking.component";

export const AdminRoutes: Routes = [
    {
        path: '',
        component: AdminDashboardComponent
    },
    {
        path: 'dashboard',
        component: AdminDashboardComponent
    },
    {
        path: 'withdraw',
        component: AdminWithdrawComponent
    },
    {
        path: 'transaction',
        component: AdminTransactionComponent
    },
    {
        path: 'agenda',
        component: AgendaComponent
    },
    {
        path: 'lending',
        component: AdminLendingComponent
    },
    {
        path: 'staking',
        component: AdminStakingComponent
    },
    {
        path: 'adminuser',
        component: AdminUserComponent
    },
    {
        path: 'users',
        component: UsersComponent
    },
    {
        path: 'users/detail/:userid',
        component: UserDetailComponent
    },
    {
        path: 'settings',
        component: SettingsComponent
    },
    {
        path: 'support',
        loadChildren: './ticket/ticket.module#AdminTicketModule'
    },
];
