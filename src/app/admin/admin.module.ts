import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {RouterModule} from "@angular/router";
import {AdminRoutes} from "./admin.routing";
import {AdminDashboardComponent} from "./dashboard/dashboard.component";
import {SharedModule} from "../shared/shared.module";
import {AdminUserComponent} from "./adminuser/adminuser.component";
import {MaterialModule} from "../app.module";
import {UsersComponent} from "./users/users.component";
import {UserDetailComponent} from "./users/detail/detail.component";
import {SettingsComponent} from "./settings/settings.component";
import {AgendaComponent} from "./agenda/agenda.component";
import {AdminLendingComponent} from "./lending/lending.component";
import {AdminWithdrawComponent} from "./withdraw/withdraw.component";
import {AdminTransactionComponent} from "./transaction/transaction.component";
import {AdminStakingComponent} from "./staking/staking.component";

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(AdminRoutes),
        SharedModule,
        MaterialModule
    ],
    declarations: [
        AdminDashboardComponent,
        AdminUserComponent,
        UsersComponent,
        UserDetailComponent,
        SettingsComponent,
        AgendaComponent,
        AdminLendingComponent,
        AdminWithdrawComponent,
        AdminTransactionComponent,
        AdminStakingComponent
    ]
})
export class AdminModule { }
